﻿#pragma warning disable
using System;
using System.IO;
using System.Linq;
using System.Xml;
using UnityEngine;

namespace NSScore
{
    public class XMLAulaController : MonoBehaviour
    {
        #region members

        private string filePathXMLAula;

        private XmlDocument xmlDocumentAula;
        #endregion

        #region properties

        public XmlDocument XMLDocumentAula
        {
            get { return xmlDocumentAula; }
        }

        #endregion

        #region MonoBehaviour

        private void Awake()
        {
            DefinePathFileXMLAula();
        }

        #endregion

        #region methods

        public void DefinePathFileXMLAula()
        {
#if UNITY_IPHONE
		filePathXMLAula = Application.persistentDataPath + "/" + "aula_conf.xml"; 
		LoadXMLAulaFileData();
#elif UNITY_ANDROID
            try
            {
                var tmpDirectoryEmulated = "/storage/emulated/0/";
                var tmpDirectorySDCard = "/storage/sdcard/";
                var tmpDirectorySDCard0 = "/storage/sdcard0/";
                var tmpDirectorySDCard1 = "/storage/sdcard1/";
                var tmpDirectorySDCardRoot = "/sdcard/";

                var tmpDirectoryInfoEmulated = new DirectoryInfo(tmpDirectoryEmulated);
                var tmpDirectoryInfoSDCard = new DirectoryInfo(tmpDirectorySDCard);
                var tmpDirectoryInfoSDCard0 = new DirectoryInfo(tmpDirectorySDCard0);
                var tmpDirectoryInfoSDCard1 = new DirectoryInfo(tmpDirectorySDCard1);
                var tmpDirectoryInfoSDCardRoot = new DirectoryInfo(tmpDirectorySDCardRoot);

                if (tmpDirectoryInfoEmulated.Exists)
                    ProcessDirectory(tmpDirectoryInfoEmulated);
                else if (tmpDirectoryInfoSDCard.Exists)
                    ProcessDirectory(tmpDirectoryInfoSDCard);
                else if (tmpDirectoryInfoSDCard0.Exists)
                    ProcessDirectory(tmpDirectoryInfoSDCard0);
                else if (tmpDirectoryInfoSDCard1.Exists)
                    ProcessDirectory(tmpDirectoryInfoSDCard1);
                else if (tmpDirectoryInfoSDCardRoot.Exists)
                    ProcessDirectory(tmpDirectoryInfoSDCardRoot);
                else
                    Debug.LogError("Directory of XML Aula no exits in android", this);
            }
            catch (Exception argError)
            {
                Debug.LogError("Can Find path Directory of XML Aula in android ERROR CODE: " + argError, this);
            }
#elif UNITY_EDITOR
            filePathXMLAula = Application.dataPath + "/aula_conf.xml";
            LoadXMLAulaFileData();
#elif UNITY_STANDALONE_OSX
			filePathXMLAula = Application.dataPath + "/" + "../../aula_conf.xml"; 
			LoadXMLAulaFileData();
#elif UNITY_STANDALONE_WIN
			filePathXMLAula = Application.dataPath + "/" + "../../../../aula_conf.xml";
			LoadXMLAulaFileData();
#endif
        }

        #endregion

        private void LoadXMLAulaFileData()
        {
            try
            {
                xmlDocumentAula = new XmlDocument();
                xmlDocumentAula.Load(filePathXMLAula);
                Debug.Log("LoadXMLAulaData Done!");
            }
            catch (Exception e)
            {
                Debug.Log("LoadXMLAulaData file not exist" + e);
            }
        }

#if UNITY_ANDROID

        void ProcessDirectory(DirectoryInfo argDirectoryInfo)
        {
            var tmpArrayFiles = argDirectoryInfo.GetFiles().Where(f => f.Extension == ".xml").ToArray();

            foreach (var tmpFileName in tmpArrayFiles)
            {
                if (tmpFileName.Name.Equals("aula_conf.xml"))
                {
                    filePathXMLAula = tmpFileName.FullName;
                    break;
                }
            }

            if (!filePathXMLAula.Equals(""))
                LoadXMLAulaFileData();
        }
#endif
    }
}