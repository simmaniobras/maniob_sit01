﻿using System;
using NSSingleton;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

namespace NSTraduccionIdiomas
{
    /// <summary>
    /// Carga el idioma desde archivos XML
    /// </summary>
    public class DiccionarioIdiomas : AbstractSingleton<DiccionarioIdiomas>
    {
        #region members

        /// <summary>
        /// Diccionario para las traducciones al idioma ingles
        /// </summary>
        public Dictionary<string, string> diccionarioIngles = new Dictionary<string, string>();

        /// <summary>
        /// Diccionario para las traducciones al idioma portugues
        /// </summary>
        public Dictionary<string, string> diccionarioPortugues = new Dictionary<string, string>();
        
        public Dictionary<string, string> diccionarioTurco = new Dictionary<string, string>();

        /// <summary>
        ///  Diccionario para las traducciones al idioma Español
        /// </summary>
        public Dictionary<string, string> diccionarioEspaniol = new Dictionary<string, string>();

        /// <summary>
        /// contiene el nombre de los arhivos empezando por español seguido de ingles y asi dependeiendo del la cantidad de lenguajes
        /// </summary>
        public string[] arrayNombresArchivosDiccionarios;

        /// <summary>
        /// Idioma seleccionado actualmente
        /// </summary>
        private ListaIdiomas idiomaActual;

        /// <summary>
        /// Para notificar cuando un diccionario fue cargado
        /// </summary>
        public UnityEvent EventOnDiccionarioCargados;

        /// <summary>
        /// si esto es igual a 2 se debe notificar que todos los diccionarios fueron cargados
        /// </summary>
        private int cantidadDiccionariosCargados;
        #endregion

        #region delegates
        
        /// <summary>
        /// Delegado al que se suscribe el metodo que traduce un string
        /// </summary>
        /// <param name="argStringParaTraducir">String que se desea traducir</param>
        private delegate string delegateTraduccionString(string argNumbreTextParaTraducir);

        private delegateTraduccionString DltTraduccionString = delegate { return ""; };

        /// <summary>
        /// delegado al que se suscriben el metodo que traduce un text por su object name
        /// </summary>
        public delegate void delegateTraducirporNombreDelObjeto();

        public delegateTraducirporNombreDelObjeto DLTraducirForNameObj = delegate { };
        #endregion

        #region private methods

        /// <summary>
        ///  metodo para traducir strings a español
        /// </summary>
        /// <param name="argStringParaTraducir">Nombre del objeto que tiene el text para traducir o llave</param>
        /// <returns>String traducido al idioma actual</returns>
        private string GetTraduccionEspaniol(string argNombreTextParaTraducir)
        {
            if (diccionarioEspaniol.ContainsKey(argNombreTextParaTraducir))
                return diccionarioEspaniol[argNombreTextParaTraducir];
            
            TrackingTextoSinTraduccion(argNombreTextParaTraducir);
            return "Etiqueta : "+ argNombreTextParaTraducir + " sin traducción.";
        }
        
        
        /// <summary>
        /// Sobrecarga del metodo para traducir strings
        /// </summary>
        /// <param name="argNombreTextParaTraducir">String para traducir</param>
        /// <returns>String traducido al idioma actual</returns>
        private string GetTraduccionIngles(string argNombreTextParaTraducir)
        {
            if (diccionarioIngles.ContainsKey(argNombreTextParaTraducir))
                return diccionarioIngles[argNombreTextParaTraducir];
            
            TrackingTextoSinTraduccion(argNombreTextParaTraducir);
            return "Etiqueta : "+ argNombreTextParaTraducir + " sin traducción.";
        }
        
        /// <summary>
        /// Sobrecarga del metodo para traducir strings
        /// </summary>
        /// <param name="argStringParaTraducir">String para traducir</param>
        /// <returns>String traducido al idioma actual</returns>
        private string GetTraduccionPortugues(string argNombreTextParaTraducir)
        {
            if (diccionarioPortugues.ContainsKey(argNombreTextParaTraducir))
                return diccionarioPortugues[argNombreTextParaTraducir];

            TrackingTextoSinTraduccion(argNombreTextParaTraducir);
            return "Etiqueta : "+ argNombreTextParaTraducir + " sin traducción.";
        }
        

        /// <summary>
        /// Debug para encontrar facilmente el string que no tiene traduccion en los diccionarios
        /// </summary>
        private void TrackingTextoSinTraduccion(string argStringParaTraducir)
        {
            Debug.LogWarning("String sin traduccion : " + argStringParaTraducir);
        }

        /// <summary>
        /// metodo que llena los diccionarios con las traducciones del xml
        /// </summary>
        public void CargarDiccionarios()
        {
            AsignarTraduccionesEspaniol();
            AsignarTraduccionesTurco();
            AsignarTraduccionesIngles();
            AsignarTraduccionesPortugues();
        }

        /// <summary>
        /// Courutina para leer las traducciones de un XML
        /// </summary>
        /// <param name="NombreText">Nombre del archivo de texto.</param>
        /// <param name="argDiccionario">Diccionario al que se le van a agregar las traducciones.</param>
        private IEnumerator CouLeerTraduccionesXML(string NombreText, Dictionary<string, string> argDiccionario)
        {
            var dir = System.IO.Path.Combine(Application.streamingAssetsPath, NombreText + ".xml");
            XmlDocument reader = new XmlDocument();

           /* UnityWebRequest www = UnityWebRequest.Get(dir);
            yield return www.SendWebRequest();
            reader.LoadXml(www.downloadHandler.text);*/

#if UNITY_ANDROID
            dir = "jar:file://" + Application.dataPath + "!/assets/"+ NombreText + ".xml";
            WWW wwwfile = new WWW(dir);
            yield return wwwfile;

            if (!string.IsNullOrEmpty(wwwfile.error))            
                Debug.Log("no se encontro el archivo");   
                
           reader.LoadXml(wwwfile.text);

#elif UNITY_WEBGL
            UnityWebRequest www = UnityWebRequest.Get(dir);
            yield return www.SendWebRequest();
            reader.LoadXml( www.downloadHandler.text);
#else
            reader.LoadXml(System.IO.File.ReadAllText(dir));
            yield return null;
#endif

            XmlNodeList _list = reader.ChildNodes[0].ChildNodes;
            
            for (int i = 0; i < _list.Count; i++)
            {
                Debug.Log(_list[i].Name);
                argDiccionario.Add(_list[i].Name, _list[i].InnerXml);
            }

            cantidadDiccionariosCargados++;

            if (cantidadDiccionariosCargados == arrayNombresArchivosDiccionarios.Length)
                EventOnDiccionarioCargados.Invoke();
        }
        #endregion

        #region Traducciones Español

        /// <summary>
        ///  Agregar las traduccion para el idioma Ingles desde el español
        /// </summary>
        private void AsignarTraduccionesEspaniol()
        {
            diccionarioEspaniol.Clear();
            StartCoroutine(CouLeerTraduccionesXML(arrayNombresArchivosDiccionarios[0], diccionarioEspaniol));
        }
        #endregion

        #region Traducciones ingles

        /// <summary>
        /// Agregar las traduccion para el idioma Ingles desde el español
        /// </summary>
        private void AsignarTraduccionesIngles()
        {
            diccionarioIngles.Clear();
            StartCoroutine(CouLeerTraduccionesXML(arrayNombresArchivosDiccionarios[1], diccionarioIngles));
        }
        #endregion

        #region Traducciones portugues 

        /// <summary>
        /// Agregar las traduccion para el idioma Portugues desde el español
        /// </summary>
        private void AsignarTraduccionesPortugues()
        {
            diccionarioPortugues.Clear();
            StartCoroutine(CouLeerTraduccionesXML(arrayNombresArchivosDiccionarios[2], diccionarioPortugues));
        }
        
        private void AsignarTraduccionesTurco()
        {
            diccionarioPortugues.Clear();
            StartCoroutine(CouLeerTraduccionesXML(arrayNombresArchivosDiccionarios[3], diccionarioTurco));
        }
        #endregion

        #region public methods



        /// <summary>
        /// Metodo por medio del cual se solicita la traduccion de un string
        /// </summary>
        /// <param name="argNombreTextParaTraducir"> </param>
        /// <param name="texto">Texto que se va a traducir</param>
        public string Traducir(string argNombreTextParaTraducir)
        {
            return DltTraduccionString(argNombreTextParaTraducir);
        }

        /// <summary>
        /// Asigna un idioma
        /// </summary>
        /// <param name="argIdioma">Idioma que se usara</param>
        public void SetIdioma(ListaIdiomas argIdioma)
        {
            switch (argIdioma)
            {
                case ListaIdiomas.espaniol:
                    idiomaActual = argIdioma;
                    DltTraduccionString = GetTraduccionEspaniol;
                    DLTraducirForNameObj();
                    break;

                case ListaIdiomas.ingles:
                    idiomaActual = argIdioma;
                    DltTraduccionString = GetTraduccionIngles;
                    DLTraducirForNameObj();
                    break;

                case ListaIdiomas.portugues:
                    idiomaActual = argIdioma;
                    DltTraduccionString = GetTraduccionPortugues;
                    DLTraducirForNameObj();
                    break;
                
                case ListaIdiomas.turco:
                    idiomaActual = argIdioma;
                    DltTraduccionString = GetTraduccionTurco;
                    DLTraducirForNameObj();
                    break;
            }
        }

        private string GetTraduccionTurco(string argNombreTextParaTraducir)
        {
            if (diccionarioTurco.ContainsKey(argNombreTextParaTraducir))
                return diccionarioTurco[argNombreTextParaTraducir];

            TrackingTextoSinTraduccion(argNombreTextParaTraducir);
            return "Etiqueta : "+ argNombreTextParaTraducir + " sin traducción.";
        }

        #endregion
    }

    public enum ListaIdiomas
    {
        espaniol,
        ingles,
        portugues,
        turco
    }
}