﻿using System.Collections;
using NSActiveZones;
using UnityEngine;
using NSInterfaz;
#if !UNITY_WEBGL
#endif
using NSBoxMessage;
using NSSeguridad;


namespace NSTraduccionIdiomas
{
    /// <summary>
    /// Clase que controla la seleccion de idiomas
    /// </summary>
    public class ClsControladorIdiomas : MonoBehaviour
    {
        public PanelInterfazSeleccionLenguajeInglesEspaniol refPanelInterfazSeleccionLenguajeInglesEspaniol;

        public PanelInterfazBienvenida refPanelInterfazBienvenida;

        public bool bilingueIngEsp;


        [SerializeField] private ClsSeguridad refSeguridad;
        public enum idioma
        {
            espaniol,
            ingles,
            portugues,
            turco
        }

        public idioma IdiomaActual;

        private void Start()
        {
            DiccionarioIdiomas.CreateInstance();
            DiccionarioIdiomas._instance.CargarDiccionarios();
            DiccionarioIdiomas._instance.SetIdioma(ListaIdiomas.turco);
            DiccionarioIdiomas._instance.EventOnDiccionarioCargados.AddListener(ActivarVentanaCorrespondiente);
        }

        private void MostrarVentanaInicial()
        {
            if (!ClsSeguridad._instance._isLogin)
                refSeguridad.mtdIniciarSimulador();

        }
        
        private void HandleDelegateOnBoxMessageButtonAccept()
        {
            Application.Quit();
        }

        private void ActivarVentanaCorrespondiente()
        {
            StartCoroutine(CouActivarPanel());
        }

        private IEnumerator CouActivarPanel()
        {
            yield return new WaitForSeconds(0.5f);

            switch (IdiomaActual)
            {
                case idioma.espaniol:
                    DiccionarioIdiomas._instance.SetIdioma(ListaIdiomas.espaniol);
                    break;

                case idioma.ingles:
                    DiccionarioIdiomas._instance.SetIdioma(ListaIdiomas.ingles);
                    break;

                case idioma.portugues:
                    DiccionarioIdiomas._instance.SetIdioma(ListaIdiomas.portugues);
                    break;
                
                case idioma.turco:
                    DiccionarioIdiomas._instance.SetIdioma(ListaIdiomas.turco);
                    break;
            }

            if (bilingueIngEsp)
                refPanelInterfazSeleccionLenguajeInglesEspaniol.Mostrar();
            else
                 refSeguridad.mtdIniciarSimulador();

        }
    }
}