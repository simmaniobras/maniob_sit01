﻿using NSUtilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSPuntaDetectora
{
    public class PuntaDetectora : MonoBehaviour
    {
        #region members

        private DraggableObjectUI refDraggableObjectUI;

        [SerializeField]
        private GameObject goBombilloIndicadorTension;
        #endregion

        private void Awake()
        {
            refDraggableObjectUI = GetComponent<DraggableObjectUI>();
            refDraggableObjectUI.DelegatePuntoAnclajeCerca = OnPuntoMedicionEncontrado;
            refDraggableObjectUI.DelegateDesanclajePunto = OnPuntaLiberada;
        }

        private void OnDisable()
        {
            refDraggableObjectUI.ResetPositionToInit();
        }

        private void OnPuntoMedicionEncontrado(GameObject argPuntoMedicion)
        {
            goBombilloIndicadorTension.SetActive(argPuntoMedicion.GetComponent<PuntoMedicionPuntaDetectora>()._tieneTension);            
        }

        private void OnPuntaLiberada()
        {
            goBombilloIndicadorTension.SetActive(false);        
        }
    }
}