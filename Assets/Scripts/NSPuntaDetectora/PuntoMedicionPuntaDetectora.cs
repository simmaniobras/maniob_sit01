﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSPuntaDetectora
{
    public class PuntoMedicionPuntaDetectora : MonoBehaviour
    {
        [SerializeField]
        private bool tieneTension;

        public bool _tieneTension
        {
            set
            {
                tieneTension = value;
            }
            get
            {
                return tieneTension;
            }
        }
    } 
}
