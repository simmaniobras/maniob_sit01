﻿using NSCelular;
using NSSituacion1;
using System.Collections;
using NSBoxMessage;
using NSInterfaz;
using UnityEngine;

namespace NSPortaFusible
{
    public class ControladorSecuenciaAnimacion : MonoBehaviour
    {
        #region members

        [SerializeField] private GameObject gameObjectCanuelaDescolgadoInicio;

        [SerializeField] private GameObject gameObjectCanuelaArreglado;

        [SerializeField] private GameObject gameObjectDesarmar;

        [SerializeField] private GameObject gameObjectFusibleNuevo;

        [SerializeField] private GameObject gameObjectFusbileQuemado;

        [SerializeField] private GameObject gameObjectPertiga;

        [SerializeField] private GameObject gameObjectTapa;

        [SerializeField] private GameObject gameObjectCanuelaDescolgadoCambio;

        [SerializeField] private GameObject gameObjectPonerCanuelaNueva;

        [SerializeField] private CortaCircuitoDesconectadoRamdom refCortaCircuitoDesconectadoRamdom;

        [SerializeField] private ControladorSituacion1 refControladorSituacion1;

        [SerializeField] private ControladorCelular refControladorCelular;

        [SerializeField] private PanelInterfazRegistroDatos refPanelInterfazRegistroDatos;

        [SerializeField] private PanelInterfazCortaCircuitos refPanelInterfazCortaCircuitos;

        [SerializeField, Header("paneles"), Space(10)]
        private GameObject panelCortaCircuitos;

        [SerializeField] private GameObject panelPortaFusible;

        [SerializeField] private GameObject panelPertigaMontarCanuelaReparada;

        [SerializeField, Header("Imagenes zonas activas")] 
        private GameObject imagenZonaActivaCanuelaDescolgadoInicio;
        
        [SerializeField]
        private GameObject imagenZonaActivaCanuelaDescolgadoCambio;
        
        [SerializeField]
        private GameObject imagenZonaActivaPertigaBajarFusible;
        
        [SerializeField]
        private GameObject imagenZonaActivaPertigaSubirFusible;
        
        [SerializeField]
        private GameObject imagenZonaActivaPonerCanuelaNueva;

        private int indiceSecuencia;

        #endregion

        public void AddIndiceSecuencia()
        {
            indiceSecuencia++;
        }

        public void BeginAnimationSecuence()
        {
            indiceSecuencia++;
            StartCoroutine(CouSecuenciaAnimacion());
        }

        public void ReiniciarSecuenciaAnimacion()
        {
            indiceSecuencia = 0;
            gameObjectCanuelaDescolgadoInicio.SetActive(true);
            gameObjectCanuelaArreglado.SetActive(false);
            gameObjectDesarmar.SetActive(false);
            gameObjectFusibleNuevo.SetActive(false);
            gameObjectFusbileQuemado.SetActive(false);
            gameObjectTapa.SetActive(false);
            gameObjectCanuelaDescolgadoCambio.SetActive(false);
            gameObjectPonerCanuelaNueva.SetActive(false);
            refControladorCelular.BorrarMensajes();
            refControladorCelular.SetMensaje1();
            refControladorCelular.SetNuevoMensajeUsuario();
            imagenZonaActivaCanuelaDescolgadoInicio.SetActive(false);
            imagenZonaActivaCanuelaDescolgadoCambio.SetActive(true);
            imagenZonaActivaPertigaBajarFusible.SetActive(true);
            imagenZonaActivaPertigaSubirFusible.SetActive(true);
            imagenZonaActivaPonerCanuelaNueva.SetActive(false);
            gameObjectPertiga.SetActive(false);

        }

        private IEnumerator CouSecuenciaAnimacion()
        {
            /*
                indiceSecuencia = 0; //no hace nada
                indiceSecuencia = 1; //Desactiva la canuela arreglada y la descuelga al final cambia por canuela retirada y sumo 1
                indiceSecuencia = 2; //no hace nada

                indiceSecuencia = 3; //Animacion retirar la tapa, quitar el fusible quemado, poner el fusible nuevo, tapar de nuevo 

                indiceSecuencia = 4; //no hace nada
                indiceSecuencia = 5; //Coloca de nuevo la canuela             
            */

            while (true)
            {
                if (indiceSecuencia == 1)
                {
                    gameObjectCanuelaDescolgadoInicio.SetActive(false);
                    gameObjectDesarmar.SetActive(true);
                    yield return new WaitForSeconds(2);
                    gameObjectDesarmar.SetActive(false);
                    gameObjectCanuelaDescolgadoCambio.SetActive(true);
                    gameObjectTapa.SetActive(true);
                    indiceSecuencia++;
                }

                if (indiceSecuencia == 3)
                {
                    var tmpAnimatorTapa = gameObjectTapa.GetComponent<Animator>();
                    tmpAnimatorTapa.SetInteger("status", 1);
                    yield return new WaitForSeconds(1.5f);
                    gameObjectFusbileQuemado.SetActive(true);
                    yield return new WaitForSeconds(1.5f);
                    gameObjectFusbileQuemado.SetActive(false);
                    gameObjectFusibleNuevo.SetActive(true);
                    yield return new WaitForSeconds(1.5f);
                    tmpAnimatorTapa.SetInteger("status", 2);
                    //gameObjectPonerCanuelaNueva.SetActive(true);
                    refControladorCelular.SetMensaje2();
                    refControladorCelular.SetNuevoMensajeUsuario();
                    panelPertigaMontarCanuelaReparada.SetActive(true);
                    indiceSecuencia++;
                }

                if (indiceSecuencia == 5)
                {
                    gameObjectTapa.SetActive(false);
                    gameObjectFusibleNuevo.SetActive(false);
                    gameObjectCanuelaDescolgadoCambio.SetActive(false);
                    gameObjectPertiga.SetActive(true);
                    yield return new WaitForSeconds(2f);
                    gameObjectPertiga.SetActive(false);
                    gameObjectCanuelaArreglado.SetActive(true);
                    panelCortaCircuitos.SetActive(true);
                    panelPortaFusible.SetActive(false);
                    refControladorSituacion1.SetEtapaCortaCircutosMalo();

                    indiceSecuencia++;

                    var tmpValueFusible = refPanelInterfazRegistroDatos.GetFusibleValue();

                    if (tmpValueFusible < 1)
                    {
                        ReiniciarSecuenciaAnimacion();
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo("Valor fusible incorrecto", "ACEPTAR");
                    }
                    else
                    {
                        refControladorCelular.SetMensaje3();
                        refControladorCelular.SetNuevoMensajeUsuario();
                        refCortaCircuitoDesconectadoRamdom.RepairAllCanuelas();
                    }
                    
                    refPanelInterfazCortaCircuitos.ImagenObscuraInteractuable(true);
                }

                yield return null;
            }
        }
    }
}