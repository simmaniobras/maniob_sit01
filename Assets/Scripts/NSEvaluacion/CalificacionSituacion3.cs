﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSEvaluacion
{
    public class CalificacionSituacion3 : MonoBehaviour
    {

        #region members

        /// <summary>
        /// Calificacion del fusible seleccionado correctamente
        /// </summary>
      //  private float fusibleSeleccionadoCorrecto;

        /// <summary>
        /// Calificación de los campos de registro de datos, cada campo tiene un peso en la calificacion, este peso es igual al resto de campos
        /// </summary>
        private float registroDatosCorrectos;

        /// <summary>
        /// Calificación de la evaluacion tipo PISA, cada pregunta tiene  4 posibles respuestas, solo una respuesta es la correcta, cada pregunta tiene un peso de calificacion igual al resto de preguntas
        /// </summary>
        private float preguntasEvaluacionCorrectas;

        /// <summary>
        /// Calificacion seleccion correcta elementos proteccion
        /// </summary>
        private float seleccionCorrectaElementosProteccion;

        /// <summary>
        /// Calificacion mensajes enviados
        /// </summary>
       // private float calificacionMensajesEnviados;

        /// <summary>
        /// Calificacion Zona seguridad
        /// </summary>
        private float calificacionZonaSeguridad;

        /// <summary>
        /// Calificación de la cantidad de intentos del usuario, cada intento resta 10% a la calificacion
        /// </summary>
        private float cantidadIntentos = 0;

        //----------------------------------------------------difernte------------------------------------------------
        
        /// <summary>
        /// realizo laimplementacion de tierra portable
        /// </summary>
        private float CalificacionUsoTierraPortable;

        /// <summary>
        /// colocacion del letrero de no operar
        /// </summary>
        private float CalificacionUsoNoOperar;

        /// <summary>
        /// esto es uno si el fusible fue cambiado
        /// </summary>
        private float CalificaiconAperturaDeFusibles;




        #endregion

        #region accesors

        /*  public float _fusibleSeleccionadoCorrecto
          {
              set { fusibleSeleccionadoCorrecto = value; }
          }*/

        public float _registroDatosCorrectos
        {
            set { registroDatosCorrectos = value; }
            get
            {
                //para habilitar el boton de reporte en el registro de datos
                return registroDatosCorrectos;
            }
        }

        public float _preguntasEvaluacionCorrectas
        {
            set { preguntasEvaluacionCorrectas = value; }
        }

        public float _seleccionCorrectaElementosProteccion
        {
            set { seleccionCorrectaElementosProteccion = value; }
        }

        public float _calificacionZonaSeguridad
        {
            set { calificacionZonaSeguridad = value; }
        }


        public float _cantidadIntentos
        {
            set { cantidadIntentos = value; }
        }
        //------------------------------------------------------------
        public float _CalificaiconAperturaDeFusibles
        {
            set { CalificaiconAperturaDeFusibles = value; }
        }

        public float _CalificacionUsoNoOperar
        {
            set { CalificacionUsoNoOperar = value; }
        }


        /*  public float _calificacionMensajesEnviados
          {
              set { calificacionMensajesEnviados = value; }
          }*/
        public float _CalificacionUsoTierraPortable
        {
            set {CalificacionUsoTierraPortable = value; }
        }

        #endregion

        #region public methods

        /// <summary>
        /// Conseguir la calificacion total 0 = 0% , 1 = 100%
        /// </summary>
        /// <returns>Calificacion total basada en los porcentajes de peso para cada "area" dentro de la situacion, las "areas" son la seleccion de la funcion de graficado con un 30% de peso, los datos registrados para la funcion de graficado seleccionada  con un 30% de peso, respuestas a las preguntas de evaluacion con un 20% de peso y numero de intentos  con un 20% de peso.</returns>
        public float GetCalificacionTotal()
        {
            float aux;

            if (cantidadIntentos > 10f)
                aux = 0;
            else
                aux = (10f - cantidadIntentos) / 10f;

            //Debug.Log("cantidad de intentos "+cantidadIntentos+"este es el resultado "+ aux)
            Debug.Log("Calificacion-----------------------------------------------------------------");
            //Debug.Log("fusibleSeleccionadoCorrecto : " + fusibleSeleccionadoCorrecto);
            Debug.Log("registroDatosCorrectos : " + registroDatosCorrectos);
            Debug.Log("preguntasEvaluacionCorrectas : " + preguntasEvaluacionCorrectas);
            Debug.Log("seleccionCorrectaElementosProteccion : " + seleccionCorrectaElementosProteccion);

            Debug.Log("CalificaiconAperturaDeFusibles : " + CalificaiconAperturaDeFusibles);
            Debug.Log("CalificacionUsoNoOperar : " + CalificacionUsoNoOperar);
            Debug.Log("CalificacionUsoTierraPortable : " + CalificacionUsoTierraPortable);

            //Debug.Log("calificacionMensajesEnviados : " + calificacionMensajesEnviados);
            //Debug.Log("calificacionZonaSeguridad : " + calificacionZonaSeguridad);
            Debug.Log("aux : " + aux);
            Debug.Log("-----------------------------------------------------------------------------");

            return (registroDatosCorrectos * 0.25f) + (preguntasEvaluacionCorrectas * 0.20f) + (seleccionCorrectaElementosProteccion * 0.15f) + (CalificaiconAperturaDeFusibles * 0.03f) + (CalificacionUsoNoOperar * 0.03f) + (CalificacionUsoTierraPortable * 0.04f) + (aux * 0.30f);//+ (calificacionZonaSeguridad * 0.1f)
        }
        #endregion


    }
}
