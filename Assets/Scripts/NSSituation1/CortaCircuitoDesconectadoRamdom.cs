﻿using NSPuntaDetectora;
using UnityEngine;

namespace NSSituacion1
{
    public class CortaCircuitoDesconectadoRamdom : MonoBehaviour
    {
        [SerializeField]
        private GameObject[] canuelaConectadas;

        [SerializeField]
        private GameObject[] canuelaDesconectadas;

        [SerializeField]
        private GameObject[] canuelaConectadasAfuera;

        [SerializeField]
        private GameObject[] canuelaDesconectadasAfuera;

        [SerializeField]
        private PuntoMedicionPuntaDetectora[] puntosMedicionPuntaDetectoraTransformador;

        [SerializeField]
        private PuntoMedicionPuntaDetectora[] puntosMedicionPuntaDetectoraSalidasCanuelas;

        /// <summary>
        /// para volver a calcular valores del sistema
        /// </summary>
        [SerializeField]
        private ControladorSituacion1 refControladorSituacion1;

        private bool canuelaDesconectadaCalculada;

        private int ramdomIndexCanuelaDesconectada;

        public int _ramdomIndexCanuelaDesconectada
        {
            get
            {
                return ramdomIndexCanuelaDesconectada;
            }
        }

        public void SetRamdomCanuelaDesconectada()
        {
            if (canuelaDesconectadaCalculada)
                return;

            ramdomIndexCanuelaDesconectada = Random.Range(0, canuelaConectadas.Length);

            for (int i = 0; i < canuelaConectadas.Length; i++)
            {
                if (i == ramdomIndexCanuelaDesconectada)
                {
                    canuelaDesconectadas[i].SetActive(true);
                    canuelaDesconectadasAfuera[i].SetActive(true);
                    puntosMedicionPuntaDetectoraTransformador[i]._tieneTension = false;
                    puntosMedicionPuntaDetectoraSalidasCanuelas[i]._tieneTension = false; 
                }
                else
                {
                    canuelaConectadasAfuera[i].SetActive(true);
                    canuelaConectadas[i].SetActive(true);
                }
            }

            canuelaDesconectadaCalculada = true;
        }

        public void RepairAllCanuelas()
        {
            for (int i = 0; i < canuelaConectadas.Length; i++)
            {
                canuelaDesconectadas[i].SetActive(false);
                canuelaConectadas[i].SetActive(true);
                canuelaDesconectadasAfuera[i].SetActive(false);
                canuelaConectadasAfuera[i].SetActive(true);
                puntosMedicionPuntaDetectoraTransformador[i]._tieneTension = true;
                puntosMedicionPuntaDetectoraSalidasCanuelas[i]._tieneTension = true;
            }

            ramdomIndexCanuelaDesconectada = -1;
            refControladorSituacion1.CalcularValoresSistema();
        }
    }
}