﻿using NSAvancedUI;
using System.Collections;
using NSInterfaz;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(CanvasGroup))]
public abstract class AbstractSingletonPanelUIAnimation<T> : MonoBehaviour where T : AbstractSingletonPanelUIAnimation<T>
{
    #region singleton

    protected static T instance;

    public static T _instance
    {
        get
        {
            if (instance == null)
            {
                var tmpObjectsOfType = Resources.FindObjectsOfTypeAll<T>();

                if (tmpObjectsOfType.Length > 0)
                    instance = tmpObjectsOfType[0];
                else
                    instance = (new GameObject(typeof(T).ToString())).AddComponent<T>();
            }

            return instance;
        }
    }

    public static void DestroySingleton()
    {
        Destroy(instance.gameObject);
    }

    public void CreateInstance()
    {
        DontDestroyOnLoad(_instance.gameObject);
    }

    #endregion

    #region members

    /// <summary>
    /// Curva de animacion de la escala del panel cuando la ventana se esta mostrando
    /// </summary>
    [SerializeField, Header("Animación panel"), Space(20), Tooltip("Curva de animacion de la escala del panel cuando la ventana se esta mostrando")]
    private SOAnimationsCurvePanelUI refSOAnimationsCurvePanelUI;

    /// <summary>
    /// Para controlar la transparencia del panel
    /// </summary>
    private CanvasGroup canvasGroup;

    [SerializeField, Tooltip("Tiempo en segundos en el que el panel debe aparecer por completo")]
    /// <summary>
    /// Tiempo en segundos en el que el panel debe aparecer por completo
    /// </summary>
    private float tiempoAparicion = 0.5f;

    [SerializeField, Tooltip("Tiempo en segundos en el que el panel debe ocultarse por completo")]
    /// <summary>
    /// Tiempo en segundos en el que el panel debe ocultarse por completo
    /// </summary>
    private float tiempoOcultacion = 0.5f;

    private IEnumerator couAparecer;

    private IEnumerator couOcultar;

    [SerializeField, Header("Panel fondo oscuro")]
    private GameObject prefabPanelImagenFondoVentanaObscura;

    private GameObject panelImagenFondoActivo;

    [SerializeField] private bool crearImagenFondoOscuro;

    [SerializeField] private bool cerrarConClickImagenFondoOscuro;

    #endregion

    #region events

    [Header("Eventos"), Space(20)]
    /// <summary>
    /// Ejecutar evento cuando el panel aparece?
    /// </summary>
    [SerializeField, Tooltip("Ejecutar evento cuando el panel aparece?")]
    private bool ejecutarEventoAparicion;

    /// <summary>
    /// Se ejecuta cuando el panel completa su aparicion
    /// </summary>
    [SerializeField, Tooltip("Se ejecuta cuando el panel completa su aparicion")]
    private UnityEvent OnPanelAparecio;

    /// <summary>
    /// Ejecutar evento cuando el panel se oculta?
    /// </summary>
    [SerializeField, Tooltip("Ejecutar evento cuando el panel se oculta?")]
    private bool ejecutarEventoOcultacion;

    /// <summary>
    /// Se ejecuta cuando el panel completa su ocultacion
    /// </summary>
    [SerializeField, Tooltip("Se ejecuta cuando el panel completa su ocultacion")]
    private UnityEvent OnPanelOculto;

    #endregion

    #region accesores

    public bool _ejecutarEventoOcultacion
    {
        set { ejecutarEventoOcultacion = value; }
    }

    public bool VentanaAbierta
    {
        get;
        set;
    }

    #endregion

    #region public methods

    public void ImagenObscuraInteractuable(bool argInteractuable)
    {
        panelImagenFondoActivo.GetComponent<PanelInterfazVentanaObscura>()._interactuable = argInteractuable;
    }
    
    /// <summary>
    /// Asigna el bool que permite que se pueda ejecutar el evento que notifica que debe ejecutarse el evento de ocultacion
    /// </summary>
    /// <param name="argEjecutarEvento">Ejecutar el evento de ocultacion? </param>
    public void EjecutarEventoOcultacion(bool argEjecutarEvento)
    {
        ejecutarEventoOcultacion = argEjecutarEvento;
    }

    private void Ocultar()
    {
        Mostrar(false);
    }

    /// <summary>
    /// Ejecuta la animacion del panel para este aparesca o se oculte
    /// </summary>
    /// <param name="argMostrar">Mostrar el panel? o ocultarlo?</param>
    public virtual void Mostrar(bool argMostrar = true)
    {
        if (canvasGroup == null)
            canvasGroup = GetComponent<CanvasGroup>();

        if (argMostrar)
        {
            if (couAparecer == null)
            {
                gameObject.SetActive(true);
                StopAllCoroutines();
                couOcultar = null;
                couAparecer = CouAparecer();
                StartCoroutine(couAparecer);

                if (crearImagenFondoOscuro)
                {
                    panelImagenFondoActivo = Instantiate(prefabPanelImagenFondoVentanaObscura, transform.parent);
                    panelImagenFondoActivo.GetComponent<Transform>().SetSiblingIndex(transform.GetSiblingIndex());

                    if (cerrarConClickImagenFondoOscuro)
                        panelImagenFondoActivo.GetComponent<PanelInterfazVentanaObscura>().DelegateClosePanel = Ocultar;
                }
            }
            else
                Debug.LogWarning("El panel ya esta ejecutando una animacion para aparecer");
        }
        else
        {
            if (!gameObject.activeSelf)
                return;

            if (couOcultar == null)
            {
                StopAllCoroutines();
                couAparecer = null;
                couOcultar = CouOcultar();
                StartCoroutine(CouOcultar());

                if (crearImagenFondoOscuro)
                    if (panelImagenFondoActivo)
                        Destroy(panelImagenFondoActivo);
            }
            else
                Debug.LogWarning("El panel ya esta ejecutando una animacion para ocultarse");
        }
    }

    #endregion

    #region courutines

    /// <summary>
    /// Courutina que ejecuta la animacion que hace aparecer el panel
    /// </summary>
    private IEnumerator CouAparecer()
    {
        Debug.Log("AbstractSingletonPanelUIAnimation CouAparecer Init");
        var tmpTiempoAnimacion = 0f;
        var tmpRectTransform = GetComponent<RectTransform>();

        while (tmpTiempoAnimacion <= tiempoAparicion)
        {
            tmpTiempoAnimacion += Time.deltaTime;
            var tmpTiempoMaximoCurvaAnimacionEscala = refSOAnimationsCurvePanelUI._animationCurveEscalaAparecer.keys[refSOAnimationsCurvePanelUI._animationCurveEscalaAparecer.keys.Length - 1].time;
            tmpRectTransform.localScale = Vector3.one * refSOAnimationsCurvePanelUI._animationCurveEscalaAparecer.Evaluate((tmpTiempoAnimacion * tmpTiempoMaximoCurvaAnimacionEscala) / tiempoAparicion);
            var tmpTiempoMaximoCurvaAnimacionAlpha = refSOAnimationsCurvePanelUI._animationCurveTransparenciaAparecer.keys[refSOAnimationsCurvePanelUI._animationCurveTransparenciaAparecer.keys.Length - 1].time;
            canvasGroup.alpha = refSOAnimationsCurvePanelUI._animationCurveTransparenciaAparecer.Evaluate((tmpTiempoAnimacion * tmpTiempoMaximoCurvaAnimacionAlpha) / tiempoAparicion);
            yield return null;
        }

        if (ejecutarEventoAparicion)
            OnPanelAparecio.Invoke();

        Debug.Log("AbstractSingletonPanelUIAnimation CouAparecer Finish");
        couAparecer = null;
        VentanaAbierta = true;
    }

    /// <summary>
    /// Courutina que ejecuta la animacion que hace ocultar el panel
    /// </summary>
    private IEnumerator CouOcultar()
    {
        Debug.Log("AbstractSingletonPanelUIAnimation CouOcultar Init");
        var tmpTiempoAnimacion = 0f;
        var tmpRectTransform = GetComponent<RectTransform>();

        while (tmpTiempoAnimacion <= tiempoOcultacion)
        {
            tmpTiempoAnimacion += Time.deltaTime;
            var tmpTiempoMaximoCurvaAnimacionEscala = refSOAnimationsCurvePanelUI._animationCurveEscalaOcultar.keys[refSOAnimationsCurvePanelUI._animationCurveEscalaOcultar.keys.Length - 1].time;
            tmpRectTransform.localScale = Vector3.one * refSOAnimationsCurvePanelUI._animationCurveEscalaOcultar.Evaluate((tmpTiempoAnimacion * tmpTiempoMaximoCurvaAnimacionEscala) / tiempoOcultacion);
            var tmpTiempoMaximoCurvaAnimacionAlpha = refSOAnimationsCurvePanelUI._animationCurveTransparenciaOcultar.keys[refSOAnimationsCurvePanelUI._animationCurveTransparenciaOcultar.keys.Length - 1].time;
            canvasGroup.alpha = refSOAnimationsCurvePanelUI._animationCurveTransparenciaOcultar.Evaluate((tmpTiempoAnimacion * tmpTiempoMaximoCurvaAnimacionAlpha) / tiempoOcultacion);
            yield return null;
        }
        
        if (ejecutarEventoOcultacion)
            OnPanelOculto.Invoke();

        couOcultar = null;
        Debug.Log("AbstractSingletonPanelUIAnimation CouOcultar Finish");
        VentanaAbierta = false;
        gameObject.SetActive(false);
    }

    #endregion
}