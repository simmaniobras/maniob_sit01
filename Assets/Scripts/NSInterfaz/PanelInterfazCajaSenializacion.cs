﻿using NSActiveZones;
using NSBoxMessage;
using NSEvaluacion;
using NSSituacion1;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace NSInterfaz
{
    public class PanelInterfazCajaSenializacion : AbstractSingletonPanelUIAnimation<PanelInterfazCajaSenializacion>
    {
        [SerializeField] private Toggle toggleLetreroAdvertencia;

        [SerializeField] private Toggle toggleValla;

        [SerializeField] private Toggle toggleBarro;

        [SerializeField] private Toggle toggleCono;

        [SerializeField] private Toggle toggleCinta;

        [SerializeField] private Toggle toggleLuz;

        [SerializeField] private ControladorDatosSesion refControladorDatosSesion;

        [SerializeField] private ActiveZonesController refActiveZonesController;

        [SerializeField] private Evaluacion refEvaluacion;

        [SerializeField, Header("Game Objects Senializacion")]
        private GameObject gameObjectSenializacionConosLetreros;

        [SerializeField] private GameObject gameObjectSenializacionVallaLuz;

        [SerializeField] private GameObject gameObjectSenializacionBarroCinta;

        public UnityEvent OnSecureZonePlaced;

        private bool ValidarSenializacion()
        {
            var tmpCantidadElementosSenializacionElegidos = 0;

            if (toggleLetreroAdvertencia.isOn)
                tmpCantidadElementosSenializacionElegidos++;

            if (toggleValla.isOn)
                tmpCantidadElementosSenializacionElegidos++;

            if (toggleBarro.isOn)
                tmpCantidadElementosSenializacionElegidos++;

            if (toggleCono.isOn)
                tmpCantidadElementosSenializacionElegidos++;

            if (toggleCinta.isOn)
                tmpCantidadElementosSenializacionElegidos++;

            if (toggleLuz.isOn)
                tmpCantidadElementosSenializacionElegidos++;

            if (tmpCantidadElementosSenializacionElegidos == 1)
            {
                BoxMessageManager._instance.MtdCreateBoxMessageInfo("Debe seleccionar la cantidad correcta de elementos de señalización.", "ACEPTAR");
                refControladorDatosSesion.AddIntentos();
                return false;
            }

            if (toggleLetreroAdvertencia.isOn && toggleCono.isOn && !toggleValla.isOn && !toggleBarro.isOn && !toggleCinta.isOn && !toggleLuz.isOn)
            {
                OnSecureZonePlaced.Invoke();

                gameObjectSenializacionConosLetreros.SetActive(true);

                refEvaluacion.AsignarCalificacionElementosZonaSeguridad(1);
                refActiveZonesController.InteractuableZone("senializacionSeguridad", false);
                return true;
            }

            if (toggleBarro.isOn && toggleCinta.isOn && !toggleValla.isOn && !toggleLuz.isOn && !toggleLetreroAdvertencia.isOn && !toggleCono.isOn)
            {
                OnSecureZonePlaced.Invoke();

                gameObjectSenializacionBarroCinta.SetActive(true);

                refEvaluacion.AsignarCalificacionElementosZonaSeguridad(1);
                refActiveZonesController.InteractuableZone("senializacionSeguridad", false);
                return true;
            }

            if (toggleValla.isOn && toggleLuz.isOn && !toggleBarro.isOn && !toggleCinta.isOn && !toggleLetreroAdvertencia.isOn && !toggleCono.isOn)
            {
                OnSecureZonePlaced.Invoke();

                gameObjectSenializacionVallaLuz.SetActive(true);

                refEvaluacion.AsignarCalificacionElementosZonaSeguridad(1);
                refActiveZonesController.InteractuableZone("senializacionSeguridad", false);
                return true;
            }

            BoxMessageManager._instance.MtdCreateBoxMessageInfo("Los elementos de señalización seleccionados son incorrectos.", "ACEPTAR");
            refControladorDatosSesion.AddIntentos();
            return false;
        }

        public void OnButtonValidarSenializacion()
        {
            if (ValidarSenializacion())
                Mostrar(false);
        }
    }
}