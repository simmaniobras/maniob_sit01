﻿using System.Collections;
using System.Collections.Generic;
using NSActiveZones;
using NSBoxMessage;
using NSSituacion5;
using NSTraduccionIdiomas;
using UnityEngine;
using UnityEngine.SceneManagement;
using ZonaActual = NSSituacion2.ZonaActual;

namespace NSInterfaz
{
    public class PanelInterfazGeneralSituacion5 : AbstractSingletonPanelUIAnimation<PanelInterfazGeneralSituacion5>
    {
        #region members

        [SerializeField] private ControladorSituacion5 refControladorSituacion5;

        [SerializeField] private ActiveZonesController refActiveZonesController;

        [SerializeField] private PanelInterfazCajaSenializacion refPanelInterfazCajaSenializacion;

        [SerializeField] private PanelInterfazCajaHerramientas refPanelInterfazCajaHerramientas;
        #endregion

        public void OnButtonHerramientas()
        {
            if (!refControladorSituacion5._avatarVestido)
            {
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeNoProteccionPersonal"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnAcceptMessageInfo);
                return;
            }

            refPanelInterfazCajaHerramientas.Mostrar();
        }

        public void OnButtonReiniciarSituacion()
        {
            BoxMessageManager._instance.MtdCreateBoxMessageDecision(DiccionarioIdiomas._instance.Traducir("mensajeReiniciarPractica"), DiccionarioIdiomas._instance.Traducir("TextCancelar"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), RestartSituationConfirm, CancelDecision);
        }

        public void OnButtonReturnBackSituation()
        {
            BoxMessageManager._instance.MtdCreateBoxMessageDecision(DiccionarioIdiomas._instance.Traducir("mensajeAbandonarPractica"), DiccionarioIdiomas._instance.Traducir("TextCancelar"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), ConfirmReturnBackSituation, CancelDecision);
        }

        private void RestartSituationConfirm()
        {
            SceneManager.LoadScene(5);
        }

        private void ConfirmReturnBackSituation()
        {
            SceneManager.LoadScene(0);
        }

        private void CancelDecision()
        {
            refActiveZonesController.ActiveAllZones();
        }

        private void OnAcceptMessageInfo()
        {
            refActiveZonesController.ActiveAllZones();
        }
    }
}