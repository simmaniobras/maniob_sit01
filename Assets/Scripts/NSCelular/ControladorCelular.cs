﻿using NSEvaluacion;
using System.Collections;
using NSTraduccionIdiomas;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NSCelular
{
    public class ControladorCelular : MonoBehaviour
    {
        #region members

        private int mensajesEnviados;

        private string circuitoID;

        private string transformadorID;

        private string apoyoID;

        [SerializeField]
        private GameObject prefabMensajeUsuario;

        [SerializeField]
        private GameObject prefabMensajeCentroMando;

        [SerializeField]
        private Transform transformContentList;

        [SerializeField]
        private ScrollRect scrollRectCelular;

        [SerializeField]
        private TextMeshProUGUI textMensajeAEnviar;

        [SerializeField]
        private Evaluacion refEvaluacion;

        [SerializeField]
        private Button buttonEnviarMensaje;

        private float calificacionEnvioMensajes;
        #endregion

        private void Awake()
        {
            IniciarCelular();
        }

        private void OnEnable()
        {
            StartCoroutine(CouControladoraMensajes());
        }

        public void IniciarCelular()
        {
            mensajesEnviados = 0;
            circuitoID = "R" + Random.Range(0, 10000).ToString("X4");
            transformadorID = "T" + Random.Range(0, 10000).ToString("X4");
            apoyoID = "M" + Random.Range(0, 100000).ToString("X5");
        }

        public void OnButtonEnviarMensaje()
        {
            var tmpPrefabMensajeUsuario = Instantiate(prefabMensajeUsuario, transformContentList);
            tmpPrefabMensajeUsuario.GetComponent<SetMessage>().SetMensaje(textMensajeAEnviar.text);
            textMensajeAEnviar.text = "";
            LayoutRebuilder.ForceRebuildLayoutImmediate(transformContentList.GetComponent<RectTransform>());
            scrollRectCelular.verticalNormalizedPosition = 0;
            mensajesEnviados++;
            buttonEnviarMensaje.interactable = false;
        }

        public void SetNuevoMensajeUsuario()
        {
            textMensajeAEnviar.text = GetMensajeParaEnviar();
            buttonEnviarMensaje.interactable = true;
        }

        public void SetMensaje1()
        {
            mensajesEnviados = 0;
        }

        public void SetMensaje2()
        {
            mensajesEnviados = 2;
        }

        public void SetMensaje3()
        {
            mensajesEnviados = 4;
        }

        private string GetMensajeParaEnviar()
        {
            var tmpMensaje = "";

            if (mensajesEnviados == 0)
                tmpMensaje = DiccionarioIdiomas._instance.Traducir("mensajeCelularSituacion1Enviar11") + circuitoID + DiccionarioIdiomas._instance.Traducir("mensajeCelularSituacion1Enviar12") + transformadorID + DiccionarioIdiomas._instance.Traducir("mensajeCelularSituacion1Enviar13") + apoyoID;
            else if (mensajesEnviados == 2)
                tmpMensaje = DiccionarioIdiomas._instance.Traducir("mensajeCelularSituacion1Enviar21") + circuitoID + DiccionarioIdiomas._instance.Traducir("mensajeCelularSituacion1Enviar22");
            else if (mensajesEnviados == 4)
                tmpMensaje = DiccionarioIdiomas._instance.Traducir("mensajeCelularSituacion1Enviar31") + transformadorID + DiccionarioIdiomas._instance.Traducir("mensajeCelularSituacion1Enviar32");

            return tmpMensaje;
        }

        private string GetMensajeCentroControl()
        {
            var tmpMensaje = "";

            if (mensajesEnviados == 1)
                tmpMensaje = DiccionarioIdiomas._instance.Traducir("mensajeCelularSituacion1CentroControl11") + circuitoID + ".";
            else if (mensajesEnviados == 3)
                tmpMensaje = DiccionarioIdiomas._instance.Traducir("mensajeCelularSituacion1CentroControl21");
            else if (mensajesEnviados == 5)
                tmpMensaje = DiccionarioIdiomas._instance.Traducir("mensajeCelularSituacion1CentroControl31") + circuitoID + ".";

            return tmpMensaje;
        }

        private IEnumerator CouControladoraMensajes()
        {
            while (true)
            {
                if (mensajesEnviados == 1 || mensajesEnviados == 3 || mensajesEnviados == 5)
                {
                    yield return new WaitForSeconds(Random.Range(1f, 2f));
                    var tmpPrefabMensajeCentroControl = Instantiate(prefabMensajeCentroMando, transformContentList);
                    tmpPrefabMensajeCentroControl.GetComponent<SetMessage>().SetMensaje(GetMensajeCentroControl());
                    LayoutRebuilder.ForceRebuildLayoutImmediate(transformContentList.GetComponent<RectTransform>());
                    calificacionEnvioMensajes += 1f / 3f;

                    if (mensajesEnviados == 5)
                        refEvaluacion.AsignarCalificacionMensajesEnviados(calificacionEnvioMensajes);

                    mensajesEnviados++;                    
                }

                yield return null;
                scrollRectCelular.verticalNormalizedPosition = 0;
            }
        }

        public void BorrarMensajes()
        {
            while (transformContentList.childCount > 0)            
                DestroyImmediate(transformContentList.GetChild(0).gameObject);            
        }
    }
}