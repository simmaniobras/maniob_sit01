﻿using NSBoxMessage;
using NSCreacionPDF;
using NSEvaluacion;
using NSInterfazAvanzada;
using NSSituacion1;
using NSTraduccionIdiomas;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NSInterfaz
{
    public class clsRegistroDatosSituacion4 : AbstractSingletonPanelUIAnimation<PanelInterfazRegistroDatos>
    {

        #region members

        // [SerializeField] private ControladorSituacion1 refControladorSituacion1;

        [SerializeField] private ControladorDatosSesion refControladorDatosSesion;

        [SerializeField] private PanelInterfazEvaluacion refPanelInterfazEvaluacion;

        [SerializeField] private ControladorValoresPDF refControladorValoresPDF;

        [SerializeField] private Button buttonReporte;

        [SerializeField] private Evaluacion refEvaluacion;


        [SerializeField, Header("Tension transformador")]
        public TMP_InputField[] inputTensionTransformador;


        [SerializeField, Header("Transformador PN")]
        public TMP_InputField[] inputTransformadorPn;

        private float valorPorCampos = 1f / 7f;

        /// <summary>
        /// se llama a la funsion que retorne este vercoto en la situacion
        /// </summary>
        private float[] DatosTensionAntes = new float[4];


        /// <summary>
        /// se llama a la funsion que retorne este vercoto en al situacion
        /// </summary>
        private float[] DatosMedidasPotencia = new float[3];


        //calificacion
        private float calificacion = 0;

        #endregion

        public void verificarTransformadoryFusible()
        {

            float tmpinputCorrienteAntesDelCambio;
            /// correinte antes del cambio del dps
            float.TryParse(inputTransformadorPn[3].text, out tmpinputCorrienteAntesDelCambio);
            var fusibleCorrecto = Mathf.Abs(tmpinputCorrienteAntesDelCambio - DatosTensionAntes[3]) == 0f;
            if (fusibleCorrecto)
            {
                Debug.Log("fusible correecto ");
                refEvaluacion.AsiganarCalidicacionTraformadorCorrecto(1f);
            }
            else {
                Debug.Log("fallo fusible");
            }

            float.TryParse(inputTensionTransformador[1].text, out tmpinputCorrienteAntesDelCambio);
            var trafoCorrecto = Mathf.Abs(tmpinputCorrienteAntesDelCambio - DatosMedidasPotencia[1]) == 0f;
            if (trafoCorrecto)
            {
                refEvaluacion.AsignarCalificacionFusibleSeleccioando(1f);
                Debug.Log("trafo bueno");
            }
            else
            {
                Debug.Log("fallo trafo");
            }

        }


        public void OnButtonValidar()
        {

            if (ValidarEmptyInputs())
            {
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeCamposNecesarios"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                return;
            }

            if (ValidarDatos())
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeValidarFelicitaciones"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
            else
            {
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeValidarIncorrectos"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                refControladorDatosSesion.AddIntentos();
            }

        }

        public void OnButtonReporte()
        {
            //Mostrar(false);
            //refControladorValoresPDF.SetPanelRegistroDatos();
           // refPanelInterfazEvaluacion.Mostrar();
            if (ValidarEmptyInputs())
            {
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeCamposNecesarios"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                return;
            }

            if (ValidarDatos())
                BoxMessageManager._instance.MtdCreateBoxMessageDecision(DiccionarioIdiomas._instance.Traducir("mensajeValidarCorrectos"), DiccionarioIdiomas._instance.Traducir("TextCancelar"), DiccionarioIdiomas._instance.Traducir("TextReporte"), OnDatosCorrectos);
            else
            {
                BoxMessageManager._instance.MtdCreateBoxMessageDecision(DiccionarioIdiomas._instance.Traducir("mensajeValidarIncorrectosReporte"), DiccionarioIdiomas._instance.Traducir("TextCancelar"), DiccionarioIdiomas._instance.Traducir("TextReporte"), OnDatosCorrectos);
                refControladorDatosSesion.AddIntentos();
            }
        }

        
        public void SetDatosTransformadorPN(float x1, float x2, float x3, float valorFusibleTipoK)
        {
            DatosTensionAntes[0] = x1;
            DatosTensionAntes[1] = x2;
            DatosTensionAntes[2] = x3;
            DatosTensionAntes[3] = valorFusibleTipoK;
        }

        public void SetDatosMedidasPotencias(float potMaximaUNidad, float potNuevoTrafo, float PotPorEncimaTrafoActual)
        {
            DatosMedidasPotencia[0] = potMaximaUNidad;
            DatosMedidasPotencia[1] = potNuevoTrafo;
            DatosMedidasPotencia[2] = PotPorEncimaTrafoActual;
        }
        
        public void verificarKvaTrafoPn()
        {
            switch (inputTensionTransformador[1].text)
            {
                case "45":

                    break;
                case "75":
             
                    break;
                case "112.5":

                    break;
                case "150":

                    break;
                case "225":

                    break;
                case "":
                    break;
                default:
                    inputTensionTransformador[1].text = "";
                    BoxMessageManager._instance.MtdCreateBoxMessageInfo("Debe ingresar la potencia comercial del transformador que se instalará en el apoyo nuevo.", "ACEPTAR");
                    break;

            }
        }

        /*public float GetFusibleValue()
        {
            if (inputValorFusible.text.Equals(""))
                return -1f;

            float tmpValueFusible;
            float.TryParse(inputValorFusible.text, out tmpValueFusible);

            if (tmpValueFusible < refControladorSituacion1._tipoFusible[refControladorSituacion1._indexTransformadorSeleccionado])
            {
                SetInputIncorrecto(inputValorFusible);
                return 0;
            }

            if (tmpValueFusible == refControladorSituacion1._tipoFusible[refControladorSituacion1._indexTransformadorSeleccionado])
                return 1;

            if (tmpValueFusible > refControladorSituacion1._tipoFusible[refControladorSituacion1._indexTransformadorSeleccionado])
                return 2;

            return 0;
        }*/

        private void OnDatosCorrectos()
        {
            Mostrar(false);
            refControladorValoresPDF.SetPanelRegistroDatos();
            refPanelInterfazEvaluacion.Mostrar();
            //buttonReporte.interactable = true;
        }

        private bool ValidarDatos()
        {
            calificacion = 0;
            //tension
            /* DatosTensionAntes = new float[]{ 1,1, 1, }; //new float[3];// se llama a la funsion que retorne este vercoto en la situacion
            DatosTensionDespues = new float[3];// se llama a la funsion que retorne este vercoto en la situacion
            //correinte 
            DatosCorrienteAntes = new float[3];// se llama a la funsion que retorne este vercoto en al situacion
            DatosCorrienteDespues = new float[3];// se llama a la funsion que retorne este vercoto en al situacion
            DatosinputPruebaPotencia = new float[4];*/

            for (int i = 0; i < inputTransformadorPn.Length; i++)
            {
                /// correinte antes del cambio del dps
                EvaluarYCompararCampo(inputTransformadorPn[i], DatosTensionAntes[i]);


            }
            for (int i = 0; i < inputTensionTransformador.Length; i++)
            {
                EvaluarYCompararCampo(inputTensionTransformador[i],DatosMedidasPotencia[i]);
            }



            Debug.Log("este es la calificacion que en el nuevo registro de datos= " + calificacion);
            refEvaluacion.AsignarCalificacionRegistroDatos(calificacion);
            if (Mathf.Approximately(calificacion, 1f))
                return true;
            else
                return false;

        }


        private void EvaluarYCompararCampo(TMP_InputField CampoAEvaluar, float DatoParaComparar)
        {
            float tmpinputCorrienteAntesDelCambio;
            /// correinte antes del cambio del dps
            float.TryParse(CampoAEvaluar.text, out tmpinputCorrienteAntesDelCambio);
            var errorCorrienteAntes = Mathf.Abs(1 - (tmpinputCorrienteAntesDelCambio * 1 / DatoParaComparar)) <= 0.05;
            if (errorCorrienteAntes)
            {
                calificacion = valorPorCampos + calificacion;
                Debug.Log("valor por cada campo= " + valorPorCampos + " valor dado " + calificacion);
                CampoAEvaluar.transform.GetChild(0).gameObject.SetActive(false);

            }
            else
            {
                CampoAEvaluar.transform.GetChild(0).gameObject.SetActive(true);
            }
        }


        private bool ValidarEmptyInputs()
        {

            var tmpEmptyInputs = 0;

            for (int i = 0; i < 3; i++)
            {
                if (inputTensionTransformador[i].text.Equals(""))
                    tmpEmptyInputs++;

            }

            for (int i = 0; i < 4; i++)
            {
                if (inputTransformadorPn[i].text.Equals(""))
                    tmpEmptyInputs++;



            }

            return tmpEmptyInputs > 0;

        }

        private void ResetInputsIncorrectos()
        {
            for (int i = 0; i < 3; i++)
            {
                inputTensionTransformador[i].transform.GetChild(0).gameObject.SetActive(false);
                inputTensionTransformador[i].text = "";


            }

            for (int i = 0; i < inputTransformadorPn.Length; i++)
            {
                inputTransformadorPn[i].transform.GetChild(0).gameObject.SetActive(false);
                inputTransformadorPn[i].text = "";
            }

        }

        private void SetInputIncorrecto(TMP_InputField argInputField, bool argIncorrecto = true)
        {
            argInputField.transform.Find("ImageBadInput").gameObject.SetActive(argIncorrecto);
        }


    }
}
