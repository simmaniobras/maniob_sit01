﻿using System.Collections;
using NSUtilities;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace NSPinzaVoltaje
{
    public class PinzaVoltaje : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        #region members        

        [SerializeField] private RectTransform canvasRectTransform;
        
        private RectTransform rectTransform;

        private RectTransform rectTransformPuntoMedicionVoltajeActual;

        private Vector2 posicionPuntero;

        private Vector2 posicionInicial;

        /// <summary>
        /// 0 = apagada, 1 = voltaje con pinza, 2 = puntaje con clavos
        /// </summary>
        private int modoPinza = -1;

        [SerializeField] private float minDistanceToPoint = 10;

        [SerializeField] private GameObject medidorVoltajePuntaNegro;

        [SerializeField] private GameObject medidorVoltajePuntaRojo;

        [SerializeField] private GameObject pantallaPinza;

        private GameObject[] puntosMedicionCerca;

        [SerializeField] private RectTransform rectTransformPinzaIzquierda;

        [SerializeField] private TextMeshProUGUI textVoltaje;

        [SerializeField] private TextMeshProUGUI textVoltajePantalla;

        [SerializeField] private GameObject textV;

        [SerializeField] private GameObject textA;
        
        [SerializeField] private GameObject textVPinza;
        
        [SerializeField] private GameObject textAPinza;

        [SerializeField] private GameObject[] zonasMedicion;

        [SerializeField] private GameObject panelTransformador;

        [SerializeField] private GameObject panelCambioCables;

        [SerializeField] private GameObject bombilloApagadoPinza;

        [SerializeField, Header("Sprite perilla pinza")]
        private Sprite[] perillaPinza;

        [SerializeField] private Image imagePerillaPinza;

        [SerializeField] private CanvasScale refCanvasScale;
        #endregion

        #region mono Behaviour

        private void Awake()
        {
            rectTransform = gameObject.GetComponent<RectTransform>();
            posicionInicial = rectTransform.localPosition;
        }

        private IEnumerator Start()
        {
            yield return null;
            posicionPuntero = posicionInicial;
            ResetValoresPinza();
        }

        private void OnEnable()
        {
            pantallaPinza.SetActive(true);
        }

        private void OnDisable()
        {
            pantallaPinza.SetActive(false);
            medidorVoltajePuntaNegro.SetActive(false);
            medidorVoltajePuntaRojo.SetActive(false);
        }

        private void Update()
        {
            MoverseHaciaPuntoMedicionVoltaje();
        }

        #endregion

        #region Drag

        public void OnBeginDrag(PointerEventData eventData)
        {
            ActivarZonasMedicion();
            puntosMedicionCerca = GameObject.FindGameObjectsWithTag("PuntosMedicionVoltaje");
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (modoPinza == 0)
            {
                textVoltaje.text = "0";
                textVoltajePantalla.text = "0";
            }
            else if (modoPinza == 1)
            {
                var tmpPuntoMedicionCerca = RevisarPuntoCerca(eventData.position);
                
                if (tmpPuntoMedicionCerca)
                {
                    if (rectTransformPuntoMedicionVoltajeActual)
                        if (rectTransformPuntoMedicionVoltajeActual != tmpPuntoMedicionCerca)
                            rectTransformPuntoMedicionVoltajeActual.GetComponent<PuntoMedicion>().ActivarCableRemplazo(false);

                    rectTransformPuntoMedicionVoltajeActual = tmpPuntoMedicionCerca;
                    rectTransformPuntoMedicionVoltajeActual.GetComponent<PuntoMedicion>().ActivarCableRemplazo();
                }
                else
                {
                    if (rectTransformPuntoMedicionVoltajeActual)
                    {
                        rectTransformPuntoMedicionVoltajeActual.GetComponent<PuntoMedicion>().ActivarCableRemplazo(false);
                        rectTransformPuntoMedicionVoltajeActual = null;
                    }
                }

                RectTransformUtility.ScreenPointToLocalPointInRectangle(canvasRectTransform, eventData.position, null, out posicionPuntero);
            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            ActivarZonasMedicion(false);
            posicionPuntero = posicionInicial;
        }

        #endregion

        public void OnButtonCambiarModo()
        {
            modoPinza++;

            if (panelTransformador)
            {
                if (panelTransformador.activeSelf || panelCambioCables)
                    modoPinza %= 3;
                else
                    modoPinza %= 2;
            }
            else
                modoPinza %= 3;

            imagePerillaPinza.sprite = perillaPinza[modoPinza];
            medidorVoltajePuntaNegro.SetActive(modoPinza == 2);
            medidorVoltajePuntaRojo.SetActive(modoPinza == 2);
            bombilloApagadoPinza.SetActive(modoPinza == 0);
            rectTransformPuntoMedicionVoltajeActual = null;

            if (modoPinza == 0)
            {
                textV.SetActive(false);
                textA.SetActive(false);
                textVPinza.SetActive(false);
                textAPinza.SetActive(false);
                textVoltaje.gameObject.SetActive(false);
                textVoltajePantalla.gameObject.SetActive(false);
            }
            else if (modoPinza == 1)
            {
                textV.SetActive(false);
                textA.SetActive(true);
                textVPinza.SetActive(false);
                textAPinza.SetActive(true);
                textVoltaje.gameObject.SetActive(true);
                textVoltajePantalla.gameObject.SetActive(true);
            }
            else if (modoPinza == 2)
            {
                textV.SetActive(true);
                textA.SetActive(false);
                textVPinza.SetActive(true);
                textAPinza.SetActive(false);
                textVoltaje.gameObject.SetActive(true);
                textVoltajePantalla.gameObject.SetActive(true);
            }

            Debug.Log("modoPinza : " + modoPinza);
        }

        public void ResetValoresPinza()
        {
            modoPinza = 0;
            posicionPuntero = posicionInicial;
            imagePerillaPinza.sprite = perillaPinza[modoPinza];
            medidorVoltajePuntaNegro.SetActive(false);
            medidorVoltajePuntaRojo.SetActive(false);
            bombilloApagadoPinza.SetActive(modoPinza == 0);
            rectTransformPuntoMedicionVoltajeActual = null;
            textVoltaje.gameObject.SetActive(false);
            textVoltajePantalla.gameObject.SetActive(false);
            textV.SetActive(false);
            textA.SetActive(false);
            textVPinza.SetActive(false);
            textAPinza.SetActive(false);            
        }

        public void SetPunteroInicial()
        {
            posicionPuntero = posicionInicial;
        }
        
        private void MoverseHaciaPuntoMedicionVoltaje()
        {
            if (modoPinza == 0)
            {
                rectTransform.localPosition = Vector3.Lerp(rectTransform.localPosition, posicionPuntero, 0.5f);
                rectTransformPinzaIzquierda.localRotation = Quaternion.Lerp(rectTransformPinzaIzquierda.localRotation, Quaternion.identity, 0.2f);
                return;
            }

            if (modoPinza == -1)
                return;
            
            if (rectTransformPuntoMedicionVoltajeActual && modoPinza == 1)
            {
                var tmpPuntoMedicionVoltaje = rectTransformPuntoMedicionVoltajeActual.GetComponent<PuntoMedicion>();

                rectTransformPinzaIzquierda.localRotation = Quaternion.Lerp(rectTransformPinzaIzquierda.localRotation, Quaternion.Euler(0, 0, tmpPuntoMedicionVoltaje._anguloAgarrePinza), 0.2f);
                Vector2 tmpPosicionDesface = tmpPuntoMedicionVoltaje._posicionDeDesface;
                tmpPosicionDesface[0] *= refCanvasScale.CanvasWidthFactor;
                tmpPosicionDesface[1] *= refCanvasScale.CanvasHeightFactor;

                Vector2 tmpPositionPinza;
                
                RectTransformUtility.ScreenPointToLocalPointInRectangle(canvasRectTransform, rectTransformPuntoMedicionVoltajeActual.position, null, out tmpPositionPinza);
                
                rectTransform.localPosition = Vector3.Lerp(rectTransform.localPosition,  tmpPositionPinza + tmpPosicionDesface, 0.2f);
                rectTransform.transform.localScale = Vector3.Lerp(rectTransform.transform.localScale, Vector3.one * tmpPuntoMedicionVoltaje._scalePinza, 0.2f);

                Debug.Log("_valorMedicion : " + tmpPuntoMedicionVoltaje._valorMedicion);
                string tmpVoltajeText = tmpPuntoMedicionVoltaje._valorMedicion.ToString("0.000");

                if (tmpPuntoMedicionVoltaje._valorMedicion == 1)
                    tmpVoltajeText = "0";

                textVoltaje.text = tmpVoltajeText;
                textVoltajePantalla.text = tmpVoltajeText;
                tmpPuntoMedicionVoltaje.ActivarCableRemplazo();
            }
            else if(modoPinza == 2)
            {
                rectTransformPinzaIzquierda.localRotation = Quaternion.Lerp(rectTransformPinzaIzquierda.localRotation, Quaternion.Euler(0, 0, 0), 0.2f);
                rectTransform.localPosition = Vector3.Lerp(rectTransform.localPosition, posicionPuntero, 0.5f);
                rectTransform.transform.localScale = Vector3.Lerp(rectTransform.transform.localScale, Vector3.one, 0.2f);
            }            
            else
            {
                rectTransformPinzaIzquierda.localRotation = Quaternion.Lerp(rectTransformPinzaIzquierda.localRotation, Quaternion.Euler(0, 0, 20), 0.2f);
                rectTransform.localPosition = Vector3.Lerp(rectTransform.localPosition, posicionPuntero, 0.5f);
                rectTransform.transform.localScale = Vector3.Lerp(rectTransform.transform.localScale, Vector3.one, 0.2f);
                textVoltaje.text = "0";
                textVoltajePantalla.text = "0";
            }
        }

        private RectTransform RevisarPuntoCerca(Vector3 argPosition)
        {
            foreach (var tmpPoint in puntosMedicionCerca)
            {
                var tmpPointRectTransform = tmpPoint.GetComponent<RectTransform>();

                if (Vector2.Distance(argPosition, tmpPointRectTransform.position) <= minDistanceToPoint)
                    return tmpPointRectTransform;
            }

            return null;
        }

        private void ActivarZonasMedicion(bool argActivar = true)
        {
            foreach (var tmpZonaMedicion in zonasMedicion)
                if (tmpZonaMedicion)
                    tmpZonaMedicion.SetActive(argActivar);
        }

        public void RotarPinza(float argRotation)
        {
            transform.rotation = Quaternion.Euler(0, 0, argRotation);
        }
    }
}