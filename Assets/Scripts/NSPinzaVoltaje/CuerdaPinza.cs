﻿using System;
using System.Collections;
using System.Collections.Generic;
using NSUtilities;
using UnityEngine;

public class CuerdaPinza : MonoBehaviour
{
    #region members
    /// <summary>
    /// Color de la cuerda
    /// </summary>
    public Color colorRope;
       

    /// <summary>
    /// Transform en el canvas desde donde comienza la cuerda
    /// </summary>
    public Transform transformStart;

    /// <summary>
    /// Transform en el canvas en donde termina la cuerda
    /// </summary>
    public Transform transformEnd;

    /// <summary>
    /// cantidad de sub divisiones de la cuerda
    /// </summary>
    public int numPoints = 10;
    
    /// <summary>
    /// fuerza de tension de la cuerda entre cada division
    /// </summary>
    public float forceMax = 200;

    /// <summary>
    /// fuerza minima de tension de la cuerda entre cada division
    /// </summary>
    public float forceMin;

    /// <summary>
    /// Distancia minima entre sub divisiones
    /// </summary>
    public float dstMin;

    /// <summary>
    /// Distancia maxima entre sub divisiones
    /// </summary>
    public float dstMax;

    private Vector2[] points;

    /// <summary>
    /// gravedad aplicada para todas las sudivion
    /// </summary>
    public int gravityForEachSubdivision = 30;

    private Vector2 startOld;

    private Vector2 endOld;

    private LineRenderer lineRenderer;

    #endregion

    private Vector2 _start
    {
        get
        {
            return Camera.main.ScreenToWorldPoint(transformStart.position);
        }
    }

    private Vector2 _end
    {
        get
        {
            return Camera.main.ScreenToWorldPoint(transformEnd.position);
        }
    }
    

    // Use this for initialization
    private void OnEnable()
    {
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.startColor = colorRope;
        lineRenderer.endColor = colorRope;

        lineRenderer.positionCount = numPoints;
        points = new Vector2[numPoints];

        for (var i = 0; i < numPoints; i++)
        {
            var p = i / (numPoints - 1f);
            points[i] = Vector2.Lerp(_start, _end, p);
        }

        for (var i = 0; i < 200; i++)
            UpdateRope();
    }

    private void FixedUpdate()
    {
        UpdateRope();

        for (var i = 0; i < lineRenderer.positionCount; i++)
            lineRenderer.SetPosition(i, points[i]);
    }

    private void UpdateRope()
    {
        var t = Mathf.InverseLerp(dstMin, dstMax, (_start - _end).magnitude);
        var F = Mathf.Lerp(forceMin, forceMax, t);
        points[0] = _start;
        points[points.Length - 1] = _end;

        for (int ik = 0; ik < gravityForEachSubdivision; ik++)
        {
            for (int i = 1; i < points.Length - 1; i++)
            {
                var offsetPrev = points[i - 1] - points[i];
                var offsetNext = points[i + 1] - points[i];
                
                var velocity = offsetPrev.magnitude * F * offsetPrev.normalized + offsetNext.magnitude * F * offsetNext.normalized;
                points[i] += velocity * Time.deltaTime / gravityForEachSubdivision;
            }
            
            for (int i = 1; i < points.Length - 1; i++)
                points[i] += Vector2.down * 9.8f * Time.deltaTime / gravityForEachSubdivision;
        }
    }
}