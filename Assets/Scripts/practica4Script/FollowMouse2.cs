﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowMouse2 : MonoBehaviour {

    #region members

    [SerializeField]
    private bool followMouse;

    private RectTransform meRectTransform;

    private Vector2 initPosition;
    #endregion

    #region accesors

    public bool _followMouse
    {
        get
        {
            return followMouse;
        }
        set
        {
            followMouse = value;
        }
    }
    #endregion

    #region monoBehaviur

    private void Awake()
    {
        meRectTransform = GetComponent<RectTransform>();
        initPosition = meRectTransform.localPosition;
    }

    private void Update()
    {
        FollowMouseExecute();
    }
    #endregion

    #region private methods

    private void FollowMouseExecute()
    {
        if (followMouse)
            meRectTransform.position = Vector2.Lerp(meRectTransform.position, Input.mousePosition, 0.85f);
        else
            meRectTransform.localPosition = Vector2.Lerp(meRectTransform.position, initPosition, 0.85f);
    }

    #endregion
    # region Public methods
    public void ActivarSeguirMouse(bool activar)
    {
        followMouse = activar;
    }
    #endregion
}
