﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace NSActiveZones
{
    public class CheckCorrectTogglesActivate : MonoBehaviour
    {
        #region members

        [SerializeField] private List<Toggle> listAllToggles;

        [SerializeField] private List<TogglesGroupVerify> listAllTogglesGroupdVerify;

        [Header("Eventos verificacion"), Space(5)]
        public UnityEvent OnTogglesVerified;

        public UnityEvent OnTogglesIncorrect;

        #endregion

        public bool VerifyGroupToggles(string argNameGroup)
        {
            var tmpResult = listAllTogglesGroupdVerify.Find(x => x.name.Equals(argNameGroup)).VerifyTogglesActive(listAllToggles);

            if (tmpResult)
                OnTogglesVerified.Invoke();
            else
                OnTogglesIncorrect.Invoke();

            return tmpResult;
        }

        public void ActiveGroupToggles(string argNameGroup)
        {
            listAllTogglesGroupdVerify.Find(x => x.name.Equals(argNameGroup)).ActiveTogglesGroups();
        }

        public void DesactivateAllToggles()
        {
            foreach (var tmpToggle in listAllToggles)
                tmpToggle.isOn = false;
        }

        public bool AnyToggleActive()
        {
            foreach (var tmpToggle in listAllToggles)
                if (tmpToggle.isOn)
                    return true;

            return false;
        }
    }

    [Serializable]
    public class TogglesGroupVerify
    {
        public string name;

        public List<Toggle> listTogglesVerify;

        public List<bool> toggleIsRequeriment;

        public UnityEvent OnTogglesActives;

        public bool VerifyTogglesActive(List<Toggle> argListAllToggles)
        {
            var tmpToggleGroupVerified = true;
            bool tmpToggleIsInList;
            
            for (int i = 0; i < argListAllToggles.Count; i++)//para todos los toggles existentes
            {
                if (argListAllToggles[i].isOn)//miro si algun toggle esta encendido
                {
                    tmpToggleIsInList = false;//
                    
                    for (int j = 0; j < listTogglesVerify.Count; j++)//para todos los toggles del grupo que se va a verificar
                    {
                        if (argListAllToggles[i] == listTogglesVerify[j])//encuentro el que esta encendido en el grupo de verificacion
                        {
                            if (toggleIsRequeriment[j])//si es un toggle obligatorio de estar encendido
                                tmpToggleGroupVerified = tmpToggleGroupVerified && listTogglesVerify[j].isOn;//un and para decir que el grupo de toggles si esta activado

                            tmpToggleIsInList = true;
                            break;
                        }
                    }

                    if (!tmpToggleIsInList)
                        tmpToggleGroupVerified = false;
                }
                else
                {
                    for (int j = 0; j < listTogglesVerify.Count; j++)//para todos los toggles del grupo que se va a verificar
                    {
                        if (argListAllToggles[i] == listTogglesVerify[j])//encuentro el que esta encendido en el grupo de verificacion
                        {
                            if (toggleIsRequeriment[j])//si es un toggle obligatorio de estar encendido
                                tmpToggleGroupVerified = false;//un and para decir que el grupo de toggles si esta activado

                            break;
                        }
                    }
                }
            }

            if (tmpToggleGroupVerified)
                OnTogglesActives.Invoke();

            return tmpToggleGroupVerified;
        }

        public void ActiveTogglesGroups(bool argActive = true)
        {
            foreach (var tmpToggle in listTogglesVerify)
                tmpToggle.isOn = argActive;
        }
    }
}