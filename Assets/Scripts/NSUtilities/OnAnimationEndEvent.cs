﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace NSUtilities
{
    public class OnAnimationEndEvent : MonoBehaviour
    {
        public UnityEvent OnAnimationEnd;

        public void TriggerAnimationEnd()
        {
            OnAnimationEnd.Invoke();
        }
    } 
}
