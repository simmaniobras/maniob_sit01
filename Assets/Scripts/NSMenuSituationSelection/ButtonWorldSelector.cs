﻿using NSUtilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSMenuSituationSelection
{
    public class ButtonWorldSelector : MonoBehaviour 
    {
        [SerializeField]
        private LayerMask layerButtonWorld;

        // Update is called once per frame
        void Update()
        {
            SelectButton();
        }

        private void SelectButton()
        {
            var tmpMousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            var tmpCollisionButton = Physics2D.OverlapCircle(tmpMousePosition, 0.1f, layerButtonWorld);

            if (tmpCollisionButton)
            {
                if (Input.GetMouseButtonDown(0))
                    tmpCollisionButton.GetComponent<IButtonWorld>().OnClickDown();
            }
        }
    }
}