using NSActiveZones;
using NSSeguridad;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using NSActiveZones;

namespace NSMenuSituationSelection
{
    public class SeleccionSituacion : MonoBehaviour
    {
        #region members
        private bool SeInicio;
      
        #endregion

        #region accesors

        #endregion

        #region mono behaviour

        private void Awake()
        {

        }

        public void Start()
        {
            SeInicio = false;
            SeleccionSituacion1();
        }
        #endregion

        #region public methods

        public void SeleccionSituacion1()
        {
            SceneManager.LoadScene("Situation1",LoadSceneMode.Additive);
        }

        public void activarBienvenida()
        {
            if (!SeInicio)
            {
                GameObject.FindGameObjectWithTag("control").GetComponent<BienvenidaControl>().ActivarBienvinida();
                SeInicio = true;
            }
            else
            {
                StartCoroutine(Activarpractica());
            }
        }

        private IEnumerator Activarpractica()
        {
            SceneManager.UnloadSceneAsync("Situation1");
            SceneManager.LoadScene("Situation1", LoadSceneMode.Additive);
            yield return new WaitForSeconds(2);
            GameObject.FindGameObjectWithTag("activarZonas").GetComponent<ActiveZonesController>().ActiveAllZones();

        }
        #endregion

    }
}