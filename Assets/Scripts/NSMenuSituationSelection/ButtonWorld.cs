﻿using NSUtilities;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace NSMenuSituationSelection
{
    [RequireComponent(typeof(Animator))]
    public class ButtonWorld : MonoBehaviour, IButtonWorld
    {
        #region members

        private Animator refAnimator;

        [SerializeField]
        private float delay;

        [SerializeField]
        private string sceneName;

        public UnityEvent onButtonClickDown;
        #endregion

        #region mono behaviour

        private void Awake()
        {
            refAnimator = GetComponent<Animator>();
        }

        private IEnumerator Start()
        {
            if (refAnimator)
            {
                while (true)
                {
                    yield return new WaitForSeconds(delay);
                    refAnimator.SetBool("Fade", true);
                    //yield return new WaitForSeconds(6f- delay);
                    yield return new WaitForSeconds(1);
                    refAnimator.SetBool("Fade", false);
                }
            }

            yield return null;            
        }
        #endregion

        #region interface

        public void OnClickDown()
        {
            if (enabled)
            {
                onButtonClickDown.Invoke();

                if (!sceneName.Equals(""))
                    SceneManager.LoadScene(sceneName);
            }
        }
        #endregion
    }

}