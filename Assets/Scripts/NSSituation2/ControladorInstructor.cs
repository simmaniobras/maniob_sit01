﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace NSSituacion2
{
    public class ControladorInstructor : MonoBehaviour
    {
        #region members

        [SerializeField]
        private TextMeshProUGUI textMensajeInstructor;

        [SerializeField]
        private Animator refAnimatorPanelMensajesInstructor;
        #endregion

        #region public methods

        public void MostrarMensajeInstructor(string  argMessageInstructor)
        {
            textMensajeInstructor.text = argMessageInstructor;
            refAnimatorPanelMensajesInstructor.SetBool("Mostrar", true);
        }

        public void OnButtoHiddePanelInstructor()
        {
            refAnimatorPanelMensajesInstructor.SetBool("Mostrar", !refAnimatorPanelMensajesInstructor.GetBool("Mostrar"));
        }
        #endregion
    }
}