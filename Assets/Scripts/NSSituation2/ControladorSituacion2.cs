﻿using NSActiveZones;
using NSBoxMessage;
using NSCelular;
using NSInterfaz;
using NSUtilities;
using System;
using System.Collections;
using NSSituacionGeneral;
using NSSituation2;
using NSTraduccionIdiomas;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace NSSituacion2
{
    public class ControladorSituacion2 : AbstractControladorSituacion
    {
        #region members

        private bool zonaSeguridadColocada;

        private bool avatarVestido;

        [SerializeField] private Button buttonCelular;

        [SerializeField] private ControladorCelularSituacion2 refControladorCelularSituacion2;

        [SerializeField] private ActiveZonesController refActiveZonesController;

        [SerializeField] private clsRegistroDatosSituacion2 refRegistroDatosSituacion2;

        [SerializeField] private byte mensajesEnviadosCentroControl;

        [SerializeField] private ZonaActual zonaActual = ZonaActual.Null;

        [SerializeField, Header("Valores transformador")]
        private Transformador[] arrayTransformadores;

        [SerializeField] private Image imageTransformador;

        private int indexTransformadorSeleccionado;

        private float[] corrienteCablesTransformador = new float[3]; //corriente X1 X 2 x3

        private float[] corrienteCablesMacromedidor = new float[3]; //corriente, amarillo = X1, azul x2, rojo x3

        private float[] tensionLineaPuntasPinza = new float[3]; // cuando mido entre las X diferentes de 0 

        private float[] tensionFasePuntasPinza = new float[3]; // cuando la roja o la negra esta en X 0 y las otra esta en cualquier X

        private float[] potenciaMacroMedidor = new float[3];

        private float corrientePantallaMacroMedidor;

        private float potenciaTransformadorActual;

        private float potenciaRegistradaExacta;

        private float potenciaRegistradaCalculada;

        private float potenciaRed;

        private float eficienciaRegistroAletorio;

        private float eficienciaRegistroCalculado;

        private float cantidadPulsos;

        private float cantidadSegundosPulsos;


        /// <summary>
        /// 0 = registro datos antes, 1 = registro datos despues
        /// </summary>
        private int etapaRegistroDatos;

        [SerializeField] private TMP_InputField inputTextSegundosPulsos;

        [SerializeField] private TextMeshProUGUI textPantallaMacroMedidor;

        [SerializeField] private ControladorTensionCortaCircuitos refControladorTensionCortaCircuitos;

        [SerializeField] private ControladorTensionCortaCircuitos refControladorTensionEstribos;

        [SerializeField] private ControladorTensionDPS refControladorTensionDPS;

        [SerializeField] private ControladorTensionTransformador refControladorTensionTransformador;

        [SerializeField] private ControladorInstructor refControladorInstructor;

        private bool lluviaRamdomEjecutada;

        private bool estaLloviendo;

        #endregion

        #region Herramientas

        [Header("Herramientas"), Space(10)] [Header("Descolgar corta circuitos"), Space(10)] [SerializeField]
        private GameObject pertigaDescolgarFusibles;

        [SerializeField] private GameObject pertigaMensajeNoOperar;

        [SerializeField] private GameObject letreroNoOperar;

        [SerializeField] private GameObject pertigaQuitarLetrero;

        [SerializeField] private GameObject pertigaColgarFusibles;

        [Header("Arreglo DPS"), Space(10)] [SerializeField]
        private GameObject llaveInglesa;

        [SerializeField] private GameObject escaleraDps;

        [SerializeField] private DraggableObjectUI refDraggableObjectUIArnesAnclaje;

        [SerializeField] private GameObject puntoArnes;

        private bool arnesAsegurado;

        [SerializeField] private bool DPSReparado;

        [SerializeField] private ControladorArnes refControladorArnes;

        [SerializeField] private ParticleSystem refParticleSystemLluvia;

        [Header("Puesta tierra"), Space(10)] [SerializeField]
        private GameObject pertigaPinzas;

        [SerializeField] private GameObject electrodoPuestaTierra;

        [SerializeField] private GameObject quitarPinzasZonaActiva;

        [SerializeField] private GameObject puestaTierraAfuera;

        [SerializeField] private GameObject pinzasZonasActivas;

        [SerializeField] private GameObject picaPuestaTierra;

        [Header("Herramientas recurrentes"), Space(10)] [SerializeField]
        private GameObject puntaDetectoraTension;

        [SerializeField] private GameObject pinzaAmperimetro;

        [SerializeField] private CheckCorrectTogglesActivate refCheckCorrectTogglesActivate;

        /// <summary>
        /// para saber en que etapa del corta circuito se va en el uso de herramientas
        /// </summary>
        private int etapaHerramientasCortaCircuitos;

        /// <summary>
        /// para saber en que etapa de la puesta a tierra se va, si en colocar el electrodo o las pinzas
        /// </summary>
        private int etapaHerramientasPuestaTierra;

        #endregion

        #region animaciones

        [Header("Animators"), Space(10)] [SerializeField]
        private Animator animatorCortaCircuitos;

        [Header("Animators controller"), Space(10)] [SerializeField]
        private RuntimeAnimatorController animatorControllerDescolgarCortaCircuitos;

        [SerializeField] private RuntimeAnimatorController animatorControllerColgarCortaCircuitos;

        #endregion

        #region zonas activas

        [Header("Zonas Activas")] [SerializeField]
        private GameObject zonasActivasDescolgarFusibles;

        [SerializeField] private GameObject zonasActivasColgarFusibles;

        [SerializeField] private GameObject zonaActivaLetreroNoOperar;

        [SerializeField] private GameObject zonaActivaQuitarLetreroNoOperar;

        #endregion

        #region cortaCircuitos

        [Header("Corta circuitos afuera")] [SerializeField]
        private GameObject[] cortaCircuitosColgadosAfuera;

        [SerializeField] private GameObject[] cortaCircuitosDescolgadosAfuera;

        [SerializeField] private GameObject puntosColgarCortaCircuitos;

        [SerializeField] private GameObject puntosDescolgarCortaCircuitos;

        [SerializeField] private GameObject pertigaColgarAvisoNoOperar;

        [SerializeField] private GameObject pertigaDescolgarAvisoNoOperar;

        #endregion

        #region Estribos

        [Header("Estribos")] [SerializeField] private GameObject goPertiga;

        [SerializeField] private GameObject goEstriboDesarmado;

        [SerializeField] private GameObject goEstriboArmado;

        [SerializeField] private GameObject goPuntoTriggerColgarEstribo;

        [SerializeField] private GameObject goPuntoTriggerDescolgarEstribo;

        [SerializeField] private GameObject goEstribosAfueraColgados;

        [SerializeField] private GameObject goEstribosAfueraDescolgados;

        [SerializeField] private ExecuteAnimation refExecuteAnimationStribos;

        #endregion

        #region accesores

        public int _indexTransformadorSeleccionado
        {
            get { return indexTransformadorSeleccionado; }
        }

        public bool _zonaSeguridadColocada
        {
            set
            {
                zonaSeguridadColocada = value;

                var tmpPreparadoSituacion = zonaSeguridadColocada && avatarVestido;

                if (tmpPreparadoSituacion)
                    refControladorCelularSituacion2.SetMensaje2();

                SetEtapaZonaSeguridad();
            }
            get { return zonaSeguridadColocada; }
        }

        public bool _avatarVestido
        {
            set
            {
                avatarVestido = value;

                var tmpPreparadoSituacion = zonaSeguridadColocada && avatarVestido;

                if (tmpPreparadoSituacion)
                    refControladorCelularSituacion2.SetMensaje2();

                SetEtapaVestirAvatar();
            }
            get { return avatarVestido; }
        }

        public byte _mensajesEnviadosCentroControl
        {
            get { return mensajesEnviadosCentroControl; }
        }

        public float[] _corrienteCablesTransformador
        {
            get { return corrienteCablesTransformador; }
        }

        public float[] _corrienteCablesMacromedidor
        {
            get { return corrienteCablesMacromedidor; }
        }

        public float[] _tensionLineaPuntasPinza
        {
            get { return tensionLineaPuntasPinza; }
        }

        public float[] _tensionFasePuntasPinza
        {
            get { return tensionFasePuntasPinza; }
        }

        public float _potenciaTransformadorActual
        {
            get { return potenciaTransformadorActual; }
        }

        public ZonaActual _etapaSituacionActual
        {
            get { return zonaActual; }
        }

        public int _etapaHerramientasPuestaTierra
        {
            get { return etapaHerramientasPuestaTierra; }
            set
            {
                etapaHerramientasPuestaTierra = value;

                if (etapaHerramientasPuestaTierra == 0)
                {
                    pertigaPinzas.SetActive(false);
                    pinzasZonasActivas.SetActive(false);
                    picaPuestaTierra.SetActive(false);
                }
            }
        }

        public float _pulsosPorSegundo
        {
            get { return cantidadPulsos; }
        }

        public float _cantidadSegundosPulsos
        {
            get { return cantidadSegundosPulsos; }
        }

        public bool _DPSReparado
        {
            set
            {
                DPSReparado = value;

                if (DPSReparado)
                    refActiveZonesController.InteractuableZone("electricbox", true);
            }
        }

        public bool ArnesAsegurado
        {
            get { return arnesAsegurado; }
        }

        #endregion

        private void Awake()
        {
            SetTransformadorRamdom();
            CalcularValoresSistema();
            refActiveZonesController.InteractuableZone("electricbox", false);
            refDraggableObjectUIArnesAnclaje.DelegatePuntoAnclajeCerca += ArnesAnclajeSeAnclo;
        }

        private void Update()
        {
            ActualizarPantallaMacroMedidor();
        }

        #region private methods

        private void ActualizarPantallaMacroMedidor()
        {
            //la punta de tension debe medir tambien en los cables del transformador
            if (!cortaCircuitosAbiertos)
                textPantallaMacroMedidor.text = corrientePantallaMacroMedidor + " KWh";
            else
                textPantallaMacroMedidor.text = "0 KWh";
        }

        private void OnButtonAceptarMensajeInformacion()
        {
            refActiveZonesController.ActiveAllZones();
        }

        private void SetTransformadorRamdom()
        {
            indexTransformadorSeleccionado = Random.Range(0, arrayTransformadores.Length);
            imageTransformador.sprite = arrayTransformadores[indexTransformadorSeleccionado].spriteTransformador;
        }

        /// <summary>
        /// Calcula todos los valores del sistema al azar, en el awake de este script y luego en el evento de cuando se cambian todos los dps
        /// </summary>
        public void CalcularValoresSistema()
        {
            corrientePantallaMacroMedidor = 0;
            eficienciaRegistroAletorio = Random.Range(0.9f, 1.1f);

            for (int i = 0; i < 3; i++) //X1, X2, X3
            {
                var tmpRamdomCorrienteCablesTransformador = Random.Range(0.5f, 0.7f);
                potenciaTransformadorActual = arrayTransformadores[indexTransformadorSeleccionado].potenciaKVA * 1000f; //(bien)
                corrienteCablesTransformador[i] = (potenciaTransformadorActual / 360.26656f) * tmpRamdomCorrienteCablesTransformador * Random.Range(0.98f, 1.03f);
                corrienteCablesMacromedidor[i] = (corrienteCablesTransformador[i] / 40) * Random.Range(0.98f, 1.02f); //(bien)
                tensionLineaPuntasPinza[i] = potenciaTransformadorActual / (1.732050f * (corrienteCablesTransformador[i] / tmpRamdomCorrienteCablesTransformador)); //bien
                tensionFasePuntasPinza[i] = tensionLineaPuntasPinza[i] / 1.732050f; // (bien)
                potenciaMacroMedidor[i] = tensionFasePuntasPinza[i] * corrienteCablesMacromedidor[i] * 0.95f; // se pierde %5 (bien)
                corrientePantallaMacroMedidor += potenciaMacroMedidor[i]; //suma de las potencias X1 X2 X3 (bien)
            }

            potenciaRed = (((tensionFasePuntasPinza[0] + tensionFasePuntasPinza[1] + tensionFasePuntasPinza[2]) / 3f) * (corrienteCablesMacromedidor[0] + corrienteCablesMacromedidor[1] + corrienteCablesMacromedidor[2])) / 1000f;
            potenciaRegistradaExacta = eficienciaRegistroAletorio * potenciaRed;

            if (etapaRegistroDatos == 0)
            {
                refRegistroDatosSituacion2.SetDatosCorrienteAntes(corrienteCablesTransformador[0], corrienteCablesTransformador[1], corrienteCablesTransformador[2]);
                refRegistroDatosSituacion2.SetDatosTensionAntes(_tensionFasePuntasPinza[0], _tensionFasePuntasPinza[1], _tensionFasePuntasPinza[2]);
                Debug.Log("estoy en el if 1");
            }
            else if (etapaRegistroDatos == 1)
            {
                Debug.Log("estoy en el if 2");
                refRegistroDatosSituacion2.SetDatosCorrienteDespues(corrienteCablesTransformador[0], corrienteCablesTransformador[1], corrienteCablesTransformador[2]);
                refRegistroDatosSituacion2.SetDatosTensionDespues(_tensionFasePuntasPinza[0], _tensionFasePuntasPinza[1], _tensionFasePuntasPinza[2]);
            }

            etapaRegistroDatos++;
        }

        public float CalcularCantidadPulsos()
        {
            float.TryParse(inputTextSegundosPulsos.text, out cantidadSegundosPulsos);
            cantidadPulsos = Mathf.Ceil((potenciaRegistradaExacta * 5000 * cantidadSegundosPulsos) / 3600f);
            potenciaRegistradaCalculada = (3600 * cantidadPulsos) / (5000 * cantidadSegundosPulsos);
            eficienciaRegistroCalculado = (potenciaRegistradaCalculada / potenciaRed) * 100;
            refRegistroDatosSituacion2.SetDatosinputPruebaPotencia(potenciaRed, potenciaRegistradaCalculada, 5000, eficienciaRegistroCalculado, cantidadSegundosPulsos);
            return cantidadPulsos;
        }

        #endregion

        #region public methods

        public void OnButtonZonaActivaTransformador()
        {
            Debug.Log("OnButtonZonaActivaTransformador");

            if (avatarVestido)
            {
                if (zonaSeguridadColocada)
                {
                    refActiveZonesController.DesactiveAllZones();
                    PanelInterfazTransformador._instance.Mostrar();
                    SetEtapaTransformador();
                }
                else
                    refControladorInstructor.MostrarMensajeInstructor(DiccionarioIdiomas._instance.Traducir("mensajeSenializacionNoColocada"));
            }
            else
            {
                refActiveZonesController.DesactiveAllZones();
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeVestimentaIncorrecta"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
            }
        }

        public void OnButtonZonaActivaEstribos()
        {
            if (avatarVestido)
            {
                if (zonaSeguridadColocada)
                {
                    refActiveZonesController.DesactiveAllZones();
                    PanelInterfazEstribos._instance.Mostrar();
                    SetEtapaEstribos();
                }
                else
                    refControladorInstructor.MostrarMensajeInstructor(DiccionarioIdiomas._instance.Traducir("mensajeSenializacionNoColocada"));
            }
            else
            {
                refActiveZonesController.DesactiveAllZones();
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeVestimentaIncorrecta"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
            }
        }

        public void OnButtonZonaActivaMacroMedidor()
        {
            Debug.Log("OnButtonZonaActivaMacroMedidor");

            if (avatarVestido)
            {
                if (zonaSeguridadColocada)
                {
                    if (DPSReparado)
                    {
                        refActiveZonesController.DesactiveAllZones();
                        PanelInterfazMacroMedidor._instance.Mostrar();
                        UsuarioIngresoAumentoMacromedidor();
                        SetEtapaMacroMedidor();
                    }
                    else
                        refControladorInstructor.MostrarMensajeInstructor(DiccionarioIdiomas._instance.Traducir("mensajeInstructorRepareDPS"));
                }
                else
                    refControladorInstructor.MostrarMensajeInstructor(DiccionarioIdiomas._instance.Traducir("mensajeSenializacionNoColocada"));
            }
            else
            {
                refActiveZonesController.DesactiveAllZones();
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeVestimentaIncorrecta"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
            }
        }

        public void OnButtonZonaActivaCortacircuito()
        {
            Debug.Log("OnButtonZonaActivaCortacircuito");

            if (avatarVestido)
            {
                if (!zonaSeguridadColocada)
                {
                    refControladorInstructor.MostrarMensajeInstructor(DiccionarioIdiomas._instance.Traducir("mensajeSenializacionNoColocada"));
                    return;
                }

                refActiveZonesController.DesactiveAllZones();
                PanelInterfazCortaCircuitos._instance.Mostrar();
                SetEtapaCortaCircutosMalo();
            }
            else
            {
                refActiveZonesController.DesactiveAllZones();
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeVestimentaIncorrecta"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
            }
        }

        public void OnButtonZonaActivaDPS()
        {
            Debug.Log("OnButtonZonaActivaDPS");

            if (avatarVestido)
            {
                if (!zonaSeguridadColocada)
                {
                    refControladorInstructor.MostrarMensajeInstructor(DiccionarioIdiomas._instance.Traducir("mensajeSenializacionNoColocada"));
                    return;
                }

                refActiveZonesController.DesactiveAllZones();
                PanelInterfazDPS._instance.Mostrar();

                if (estaLloviendo)
                    UsuarioPeligroElectrocucion();
                else if (!arnesAsegurado)
                    refControladorInstructor.MostrarMensajeInstructor(DiccionarioIdiomas._instance.Traducir("mensajeInstructorAseguresePuntoFijo"));

                if (!lluviaRamdomEjecutada)
                    StartCoroutine(CouLluviaRamdom());

                SetEtapaDPS();
            }
            else
            {
                refActiveZonesController.DesactiveAllZones();
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeVestimentaIncorrecta"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
            }
        }

        public void OnButtonZonaActivaPuestaTierra()
        {
            Debug.Log("OnButtonZonaActivaPuestaTierra");

            if (avatarVestido)
            {
                if (!zonaSeguridadColocada)
                {
                    refControladorInstructor.MostrarMensajeInstructor(DiccionarioIdiomas._instance.Traducir("mensajeSenializacionNoColocada"));
                    return;
                }

                refActiveZonesController.DesactiveAllZones();
                PanelInterfazPuestaTierra._instance.Mostrar();
                SetEtapaPuestaTierra();
            }
            else
            {
                refActiveZonesController.DesactiveAllZones();
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeVestimentaIncorrecta"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
            }
        }

        public void AgregarMensajesEnviados()
        {
            mensajesEnviadosCentroControl++;
        }

        public void OnButtonValidarHerramienta()
        {
            DesactivarHerramientas();

            switch (zonaActual)
            {
                case ZonaActual.Null:
                    BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeNoPuedeUsarHerramientaAqui"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
                    PanelInterfazCajaHerramientas._instance.Mostrar(false);
                    break;

                case ZonaActual.VestirAvatar:
                    BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeNoPuedeUsarHerramientaAqui"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
                    PanelInterfazCajaHerramientas._instance.Mostrar(false);
                    break;

                case ZonaActual.ZonaSeguridad:

                    break;

                case ZonaActual.MacroMedidor:
                    Debug.Log("EtapaSituacionActual.MacroMedidor");

                    if (refCheckCorrectTogglesActivate.VerifyGroupToggles("PinzaAmperimetrica"))
                    {
                        pinzaAmperimetro.SetActive(true);
                        PanelInterfazCajaHerramientas._instance.Mostrar(false);
                    }
                    else if (refCheckCorrectTogglesActivate.AnyToggleActive())
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaSeleccionadaSinFuncion"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    else
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaNoSeleccionada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));

                    break;

                case ZonaActual.CortaCircuitos:

                    if (refCheckCorrectTogglesActivate.VerifyGroupToggles("PuntaMedidoraTension"))
                    {
                        DesactivarHerramientas();
                        puntaDetectoraTension.SetActive(true);
                        PanelInterfazCajaHerramientas._instance.Mostrar(false);
                    }
                    else if (etapaHerramientasCortaCircuitos == 0)
                    {
                        Debug.Log("etapaHerramientasCortaCircuitos == 0");

                        if (refCheckCorrectTogglesActivate.VerifyGroupToggles("DescolgarCortaCircuitos"))
                        {
                            animatorCortaCircuitos.enabled = false;
                            zonasActivasDescolgarFusibles.SetActive(true);
                            pertigaDescolgarFusibles.transform.GetChild(0).GetComponent<Image>().color = Color.white; //restablesco el color de la pertiga despues de esta ser descolgada
                            pertigaDescolgarFusibles.SetActive(true);
                            pertigaDescolgarFusibles.GetComponent<FollowMouse>().enabled = true;
                            puntosDescolgarCortaCircuitos.SetActive(true);
                            PanelInterfazCajaHerramientas._instance.Mostrar(false);
                            etapaHerramientasCortaCircuitos++;
                        }
                        else if (refCheckCorrectTogglesActivate.AnyToggleActive())
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaInadecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                        else
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaSeleccionadaDesenclavarFusibles"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    }
                    else if (etapaHerramientasCortaCircuitos == 1)
                    {
                        Debug.Log("etapaHerramientasCortaCircuitos == 1");

                        if (refCheckCorrectTogglesActivate.VerifyGroupToggles("ColgarCortaCircuitos"))
                        {
                            if (etapaHerramientasPuestaTierra == 1)
                                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajePuestaTierraConectada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                            else
                            {
                                animatorCortaCircuitos.enabled = false;
                                zonasActivasColgarFusibles.SetActive(true);
                                pertigaColgarFusibles.SetActive(true);
                                puntosColgarCortaCircuitos.SetActive(true);
                                PanelInterfazCajaHerramientas._instance.Mostrar(false);
                                etapaHerramientasCortaCircuitos = 4;
                            }
                        }
                        else if (refCheckCorrectTogglesActivate.VerifyGroupToggles("PonerLetreroPeligroCortaCircuitos"))
                        {
                            animatorCortaCircuitos.enabled = false;
                            pertigaMensajeNoOperar.SetActive(true);
                            pertigaDescolgarFusibles.SetActive(false);
                            zonaActivaLetreroNoOperar.SetActive(true);
                            PanelInterfazCajaHerramientas._instance.Mostrar(false);
                            etapaHerramientasCortaCircuitos++;
                        }
                        else if (refCheckCorrectTogglesActivate.AnyToggleActive())
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaInadecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                        else
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeEscojaHerramientaAdecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    }
                    else if (etapaHerramientasCortaCircuitos == 2)
                    {
                        Debug.Log("etapaHerramientasCortaCircuitos == 2");

                        if (refCheckCorrectTogglesActivate.VerifyGroupToggles("QuitarLetreroNoOperar"))
                        {
                            animatorCortaCircuitos.enabled = false;
                            zonaActivaQuitarLetreroNoOperar.SetActive(true);
                            pertigaQuitarLetrero.SetActive(true);
                            PanelInterfazCajaHerramientas._instance.Mostrar(false);
                            etapaHerramientasCortaCircuitos++;
                        }
                        else if (refCheckCorrectTogglesActivate.AnyToggleActive())
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaInadecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                        else
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeEscojaHerramientaAdecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    }
                    else if (etapaHerramientasCortaCircuitos == 3)
                    {
                        Debug.Log("etapaHerramientasCortaCircuitos == 3");

                        if (refCheckCorrectTogglesActivate.VerifyGroupToggles("PonerLetreroPeligroCortaCircuitos"))
                        {
                            animatorCortaCircuitos.enabled = false;
                            pertigaMensajeNoOperar.SetActive(true);
                            pertigaDescolgarFusibles.SetActive(false);
                            zonaActivaLetreroNoOperar.SetActive(true);
                            PanelInterfazCajaHerramientas._instance.Mostrar(false);
                            etapaHerramientasCortaCircuitos = 2;
                        }
                        else if (refCheckCorrectTogglesActivate.VerifyGroupToggles("ColgarCortaCircuitos"))
                        {
                            if (etapaHerramientasPuestaTierra == 1)
                                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajePuestaTierraConectada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                            else
                            {
                                animatorCortaCircuitos.enabled = false;
                                zonasActivasColgarFusibles.SetActive(true);
                                pertigaColgarFusibles.SetActive(true);
                                puntosColgarCortaCircuitos.SetActive(true);
                                PanelInterfazCajaHerramientas._instance.Mostrar(false);
                                etapaHerramientasCortaCircuitos = 4;
                            }
                        }
                        else if (refCheckCorrectTogglesActivate.AnyToggleActive())
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaInadecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                        else
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeEscojaHerramientaAdecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    }

                    break;

                case ZonaActual.Transformador:

                    if (refCheckCorrectTogglesActivate.VerifyGroupToggles("PuntaMedidoraTension"))
                    {
                        DesactivarHerramientas();
                        puntaDetectoraTension.SetActive(true);
                        PanelInterfazCajaHerramientas._instance.Mostrar(false);
                    }
                    else if (refCheckCorrectTogglesActivate.VerifyGroupToggles("PinzaAmperimetrica"))
                    {
                        pinzaAmperimetro.SetActive(true);
                        PanelInterfazCajaHerramientas._instance.Mostrar(false);
                    }
                    else if (refCheckCorrectTogglesActivate.AnyToggleActive())
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaInadecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    else
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeEscojaHerramientaAdecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));

                    break;

                case ZonaActual.DPSs:

                    if (refCheckCorrectTogglesActivate.VerifyGroupToggles("PuntaMedidoraTension"))
                    {
                        DesactivarHerramientas();
                        puntaDetectoraTension.SetActive(true);
                        PanelInterfazCajaHerramientas._instance.Mostrar(false);
                    }
                    else if (refCheckCorrectTogglesActivate.VerifyGroupToggles("CambioDPS"))
                    {
                        if (estaLloviendo)
                        {
                            UsuarioPeligroElectrocucion();
                            PanelInterfazCajaHerramientas._instance.Mostrar(false);
                            return;
                        }

                        if (arnesAsegurado)
                            llaveInglesa.SetActive(true);
                        else
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo("El arnes no a sido asegurado. (Texto temporal)", DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));

                        PanelInterfazCajaHerramientas._instance.Mostrar(false);
                    }
                    else if (refCheckCorrectTogglesActivate.AnyToggleActive())
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaInadecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    else
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeEscojaHerramientaAdecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));

                    break;

                case ZonaActual.PuestaTierra:

                    if (etapaHerramientasPuestaTierra == 0)
                    {
                        if (refControladorTensionTransformador._tensionAleatoriaActivada)
                        {
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeTensionExistenteEncircuito"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                            PanelInterfazCajaHerramientas._instance.Mostrar(false);
                        }
                        else if (refCheckCorrectTogglesActivate.VerifyGroupToggles("PertigaPinzasPuestaTierra"))
                        {
                            pertigaPinzas.SetActive(true);
                            pinzasZonasActivas.SetActive(true);
                            picaPuestaTierra.SetActive(true);
                            PanelInterfazCajaHerramientas._instance.Mostrar(false);
                            etapaHerramientasPuestaTierra++;
                        }
                        else if (refCheckCorrectTogglesActivate.AnyToggleActive())
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaInadecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                        else
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeEscojaHerramientaAdecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    }
                    else if (etapaHerramientasPuestaTierra == 1)
                    {
                        if (refCheckCorrectTogglesActivate.VerifyGroupToggles("DescolgarCortaCircuitos")) //se usa este grupo de toggles porque es el que tiene la pertiga solamente para descolgar la puesta a tierra
                        {
                            pertigaPinzas.SetActive(true);
                            quitarPinzasZonaActiva.SetActive(true);
                            PanelInterfazCajaHerramientas._instance.Mostrar(false);
                            etapaHerramientasPuestaTierra++;
                        }
                        else if (refCheckCorrectTogglesActivate.AnyToggleActive())
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaInadecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                        else
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeEscojaHerramientaAdecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    }

                    break;

                case ZonaActual.Estribos:

                    if (refCheckCorrectTogglesActivate.VerifyGroupToggles("DescolgarCortaCircuitos"))
                    {
                        goPertiga.SetActive(true);
                        PanelInterfazCajaHerramientas._instance.Mostrar(false);
                    }
                    else if (refCheckCorrectTogglesActivate.AnyToggleActive())
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaInadecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    else
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeEscojaHerramientaAdecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));

                    break;

                case ZonaActual.Afuera:


                    if (refCheckCorrectTogglesActivate.VerifyGroupToggles("EscaleraDPS"))
                    {
                        escaleraDps.SetActive(true);
                        refActiveZonesController.SetZoneInteractuable("dps", true);
                        PanelInterfazCajaHerramientas._instance.Mostrar(false);
                    }
                    else if (refCheckCorrectTogglesActivate.AnyToggleActive())
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaInadecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    else
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeEscojaHerramientaAdecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));

                    break;
            }
        }

        public void DesactivarHerramientas()
        {
            puntaDetectoraTension.SetActive(false);
            pinzaAmperimetro.SetActive(false);
        }

        //set Zones
        public void SetEtapaVestirAvatar()
        {
            Debug.Log("SetEtapaVestirAvatar");
            zonaActual = ZonaActual.VestirAvatar;
        }

        public void SetEtapaZonaSeguridad()
        {
            Debug.Log("SetEtapaZonaSeguridad");
            zonaActual = ZonaActual.ZonaSeguridad;
        }

        public void SetEtapaMacroMedidor()
        {
            Debug.Log("SetEtapaMacroMedidor");
            zonaActual = ZonaActual.MacroMedidor;
            puntaDetectoraTension.SetActive(false);
            pinzaAmperimetro.SetActive(false);
        }

        public void SetEtapaVisualizacionCortaCircuito()
        {
            Debug.Log("SetEtapaVisualizacionCortaCircuito");
            zonaActual = ZonaActual.CortaCircuitos;
            puntaDetectoraTension.SetActive(false);
            pinzaAmperimetro.SetActive(false);
        }

        public void SetEtapaCortaCircutosMalo()
        {
            Debug.Log("SetEtapaCortaCircuto");
            zonaActual = ZonaActual.CortaCircuitos;
            puntaDetectoraTension.SetActive(false);
            pinzaAmperimetro.SetActive(false);
        }

        public void SetEtapaTransformador()
        {
            Debug.Log("SetEtapaTransformador");
            zonaActual = ZonaActual.Transformador;
            puntaDetectoraTension.SetActive(false);
            pinzaAmperimetro.SetActive(false);
        }

        private void SetEtapaDPS()
        {
            Debug.Log("SetEtapaDPS");
            zonaActual = ZonaActual.DPSs;
        }

        private void SetEtapaPuestaTierra()
        {
            Debug.Log("SetEtapaPuestaTierra");
            zonaActual = ZonaActual.PuestaTierra;
        }

        public void SetEtapaAfuera()
        {
            Debug.Log("SetEtapaAfuera");
            zonaActual = ZonaActual.Afuera;
        }

        public void SetEtapaEstribos()
        {
            Debug.Log("SetEtapaEstribos");
            zonaActual = ZonaActual.Estribos;
            puntaDetectoraTension.SetActive(false);
            pinzaAmperimetro.SetActive(false);
        }

        public void DesactivarZonaActivaDPS()
        {
            refActiveZonesController.SetZoneInteractuable("dps", false);
        }

        public void ActivarPuestaTierraAfuera(bool argActivar)
        {
            puestaTierraAfuera.SetActive(argActivar);

            if (!argActivar)
            {
                pinzasZonasActivas.SetActive(false);
                picaPuestaTierra.SetActive(false);
                etapaHerramientasPuestaTierra = 0;
            }
        }

        public void EjecutarAnimacionDescolgarCortaFusibles()
        {
            animatorCortaCircuitos.enabled = true;
            animatorCortaCircuitos.runtimeAnimatorController = animatorControllerDescolgarCortaCircuitos;
            animatorCortaCircuitos.SetBool("Descolgar", true);
            zonasActivasDescolgarFusibles.SetActive(false);
            puntosDescolgarCortaCircuitos.SetActive(false);
            StartCoroutine(CouDesactivarAnimatorDescolgarFusibles());
        }

        public void EjecutarAnimacionColgarCortaFusibles()
        {
            animatorCortaCircuitos.enabled = true;
            animatorCortaCircuitos.runtimeAnimatorController = animatorControllerColgarCortaCircuitos;
            animatorCortaCircuitos.SetBool("Colgar", true);
            pertigaColgarFusibles.SetActive(false);
            zonasActivasColgarFusibles.SetActive(false);
            puntosColgarCortaCircuitos.SetActive(false);
            StartCoroutine(CouDesactivarAnimatorColgarFusibles());
        }

        public void EjecutarAnimacionColgarEstribos()
        {
            goPertiga.SetActive(false);
            goEstriboDesarmado.SetActive(false);
            goPuntoTriggerColgarEstribo.SetActive(false);

            refExecuteAnimationStribos.PlayAnimacion("Colgar", () =>
            {
                goEstriboArmado.SetActive(true);
                goPuntoTriggerDescolgarEstribo.SetActive(true);
                goEstribosAfueraColgados.SetActive(true);
                goEstribosAfueraDescolgados.SetActive(false);

                refControladorTensionEstribos.ActivarTensionInferior();

                if (!cortaCircuitosAbiertos)
                {
                    refControladorTensionCortaCircuitos.ActivarTensionInferior();
                    refControladorTensionDPS.ActivarTension();
                    refControladorTensionTransformador.ActivarTension();
                }
            });
        }

        public void EjecutarAnimacionDescolgarEstribos()
        {
            goPertiga.SetActive(false);
            goEstriboArmado.SetActive(false);
            goPuntoTriggerDescolgarEstribo.SetActive(false);

            refExecuteAnimationStribos.PlayAnimacion("Descolgar", () =>
            {
                goEstriboDesarmado.SetActive(true);
                goPuntoTriggerColgarEstribo.SetActive(true);
                goEstribosAfueraColgados.SetActive(false);
                goEstribosAfueraDescolgados.SetActive(true);
                PanelInterfazEstribos._instance.Mostrar(false);

                refControladorTensionEstribos.DesactivarTensionInferior();
                refControladorTensionCortaCircuitos.DesactivarTensionInferior();
                refControladorTensionDPS.DesactivarTension();
                refControladorTensionTransformador.DesactivarTensionAleatorio();
            }, 1);
        }

        public void SetLetreroNoOperarCortaCircuitos()
        {
            Debug.Log("SetLetreroNoOperarCortaCircuitos");
            pertigaMensajeNoOperar.SetActive(false);
            letreroNoOperar.SetActive(true);
        }

        public void DesactivarTodaTension()
        {
            if (cortaCircuitosAbiertos)
            {
                refControladorTensionEstribos.DesactivarTensionInferior();
                refControladorTensionCortaCircuitos.DesactivarTensionInferior();
                refControladorTensionDPS.DesactivarTension();
                refControladorTensionTransformador.DesactivarTension();
            }
        }

        private void ArnesAnclajeSeAnclo(GameObject argObjetoAlQueSeAnclo)
        {
            UsuarioSeAncloConArnes();
            refDraggableObjectUIArnesAnclaje.transform.position = argObjetoAlQueSeAnclo.transform.position;
            refDraggableObjectUIArnesAnclaje.enabled = false;
            arnesAsegurado = true;
            refControladorArnes.ConectarArnes();
            puntoArnes.SetActive(false);
        }

        ///mensajes instructor
        public void UsuarioElectrocutado()
        {
            refControladorInstructor.MostrarMensajeInstructor(DiccionarioIdiomas._instance.Traducir("mensajeInstructorElectrocutado"));
        }

        public void UsuarioPeligroElectrocucion()
        {
            refControladorInstructor.MostrarMensajeInstructor(DiccionarioIdiomas._instance.Traducir("mensajeInstructorPeligroElectrocucion"));
        }

        public void UsuarioNoAColocadoPuestaTierra()
        {
            refControladorInstructor.MostrarMensajeInstructor(DiccionarioIdiomas._instance.Traducir("mensajeInstructorVerificarAusenciaTension"));
        }

        public void UsuarioColocoPuestaTierraMal()
        {
            refControladorInstructor.MostrarMensajeInstructor(DiccionarioIdiomas._instance.Traducir("mensajeInstructorPuestaTierraIncorrecta"));
        }

        public void UsuarioRetiroPuestaTierraMal()
        {
            refControladorInstructor.MostrarMensajeInstructor(DiccionarioIdiomas._instance.Traducir("mensajeInstructorRetiroPuestaTierra"));
        }

        public void UsuarioNoADescolgadoFusibles()
        {
            refControladorInstructor.MostrarMensajeInstructor(DiccionarioIdiomas._instance.Traducir("mensajeInstructorAperturaCircuito"));
            etapaHerramientasPuestaTierra = 0;
        }

        public void UsuarioPresionoBotonPanico()
        {
            refControladorInstructor.MostrarMensajeInstructor(DiccionarioIdiomas._instance.Traducir("mensajeInstructorBotonGeneracion"));
        }

        public void UsuarioSeAncloConArnes()
        {
            refControladorInstructor.MostrarMensajeInstructor(DiccionarioIdiomas._instance.Traducir("mensajeInstructorSeleccioneDPSLlaveInglesa"));
        }

        public void UsuarioIngresoAumentoMacromedidor()
        {
            refControladorInstructor.MostrarMensajeInstructor(DiccionarioIdiomas._instance.Traducir("mensajeInstructorRevisarMacromedidor"));
        }

        public void UsuarioEmpezoLlover()
        {
            refControladorInstructor.MostrarMensajeInstructor(DiccionarioIdiomas._instance.Traducir("mensajeInstructorEmpezoLlover"));
        }

        #endregion

        #region courutines

        private IEnumerator CouDesactivarAnimatorDescolgarFusibles()
        {
            yield return new WaitForSeconds(3);
            animatorCortaCircuitos.enabled = false;

            refControladorTensionCortaCircuitos.DesactivarTensionInferior();
            refControladorTensionDPS.DesactivarTension();
            refControladorTensionTransformador.DesactivarTensionAleatorio();

            cortaCircuitosDescolgadosAfuera[0].SetActive(true);
            cortaCircuitosDescolgadosAfuera[1].SetActive(true);

            cortaCircuitosColgadosAfuera[0].SetActive(false);
            cortaCircuitosColgadosAfuera[1].SetActive(false);

            refControladorCelularSituacion2.SetMensaje3();
            pertigaColgarFusibles.GetComponent<FollowMouse>().ResetPosition();

            cortaCircuitosAbiertos = true;
            Debug.Log("CouDesactivarAnimatorDescolgarFusibles");
        }

        private IEnumerator CouDesactivarAnimatorColgarFusibles()
        {
            yield return new WaitForSeconds(3.25f);
            animatorCortaCircuitos.enabled = false;

            if (goEstribosAfueraColgados.activeSelf)
            {
                refControladorTensionCortaCircuitos.ActivarTensionInferior();
                refControladorTensionDPS.ActivarTension();
                refControladorTensionTransformador.ActivarTension();
            }

            pertigaDescolgarFusibles.SetActive(false);

            cortaCircuitosDescolgadosAfuera[0].SetActive(false);
            cortaCircuitosDescolgadosAfuera[1].SetActive(false);

            cortaCircuitosColgadosAfuera[0].SetActive(true);
            cortaCircuitosColgadosAfuera[1].SetActive(true);
            refControladorCelularSituacion2.SetMensaje5();
            cortaCircuitosAbiertos = false;
            etapaHerramientasCortaCircuitos = 0;

            Debug.Log("CouDesactivarAnimatorColgarFusibles finish");
        }

        private IEnumerator CouLluviaRamdom()
        {
            lluviaRamdomEjecutada = true;
            yield return new WaitForSeconds(Random.Range(3f, 7f));

            if (Random.value <= 0.65f)
            {
                estaLloviendo = true;
                refParticleSystemLluvia.Play();
                UsuarioEmpezoLlover();

                var tmpLimitSeconds = Random.Range(20f, 25f);
                var tmpSecondsCount = 0f;

                while (tmpSecondsCount < tmpLimitSeconds)
                {
                    tmpSecondsCount += Time.deltaTime;

                    if (PanelInterfazDPS._instance.VentanaAbierta) //si la ventana esta abierta aun, se advierte al usuario de lectrocucion
                    {
                        UsuarioPeligroElectrocucion();
                        break; //se cancela este ciclo while
                    }

                    yield return null;
                }

                while (tmpSecondsCount < tmpLimitSeconds) //esta es similar que la anterior pero solo cumple la funcion de llegar al final de la lluvia sin avisar de nuevo sobre posible electrocucion
                {
                    tmpSecondsCount += Time.deltaTime;
                    yield return null;
                }

                estaLloviendo = false;
                refParticleSystemLluvia.Stop();
            }
        }

        #endregion
    }

    [Serializable]
    public class Transformador
    {
        public float potenciaKVA;

        public Sprite spriteTransformador;
    }

    public enum ZonaActual
    {
        Null,
        VestirAvatar,
        ZonaSeguridad,
        MacroMedidor,
        CortaCircuitos,
        DPSs,
        Transformador,
        PuestaTierra,
        Afuera,
        Estribos
    }
}