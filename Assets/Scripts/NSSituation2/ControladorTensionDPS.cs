﻿using NSPuntaDetectora;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSSituacion2
{
    public class ControladorTensionDPS : MonoBehaviour
    {
        [SerializeField]
        private PuntoMedicionPuntaDetectora[] arraysPuntosMedicion;
        
        public void DesactivarTension()
        {
            foreach (var puntoMedicion in arraysPuntosMedicion)
                puntoMedicion._tieneTension = false;
        }

        public void ActivarTension()
        {
            foreach (var puntoMedicion in arraysPuntosMedicion)
                puntoMedicion._tieneTension = true;
        }
    } 
}
