﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSBoxMessage;

namespace NSSituacion2
{
    public class ButtonPanic : MonoBehaviour
    {
        #region members

        private int intentosPresionBoton;

        [SerializeField]
        private ControladorSituacion2 refControladorSituacion2;
        #endregion

        public void OnButtonPanic()
        {
            if (intentosPresionBoton == 0)
            {
                refControladorSituacion2.UsuarioPresionoBotonPanico();
            }
            else if (intentosPresionBoton == 1)
            {
                refControladorSituacion2.DesactivarTodaTension();
            }

            intentosPresionBoton++;          
        }
    } 
}
