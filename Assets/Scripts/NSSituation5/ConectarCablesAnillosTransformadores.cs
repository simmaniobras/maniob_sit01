using NSBoxMessage;
using NSTraduccionIdiomas;
using UnityEngine;

namespace NSSituacion5
{
    public class ConectarCablesAnillosTransformadores : MonoBehaviour
    {
        #region members

        private int indexCableSeleccionado;

        [SerializeField] private GameObject[] arrayImageCablesAnilloTransformadorEnergia;
        
        [SerializeField] private GameObject[] arrayCablesDesconectados;
        
        [SerializeField] private GameObject[] arrayCablesConectados;

        [SerializeField] private Vector2[] arrayPositionsCablesAnillos;

        [SerializeField] private ControladorSituacion5 refControladorSituacion5;        
        #endregion

        #region accesor

        public bool CablesTCConectados
        {
            get { return arrayCablesConectados[0].activeSelf && arrayCablesConectados[1].activeSelf && arrayCablesConectados[2].activeSelf; }
        }

        #endregion
        
        #region public methods

        public void SeleccionarCable(int argIndexCableSeleccionado)
        {
            if (!refControladorSituacion5.CortaCircuitosAbiertos)
            {
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeCablesTCNoSePuedenConectar"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                return;
            }
            
            indexCableSeleccionado = argIndexCableSeleccionado;
            arrayCablesDesconectados[indexCableSeleccionado].SetActive(true);
            arrayImageCablesAnilloTransformadorEnergia[indexCableSeleccionado].SetActive(false);
        }

        public void ColocarCable(int argIndexAnilloColocacion)
        {
            arrayCablesDesconectados[indexCableSeleccionado].SetActive(false);
            arrayCablesConectados[indexCableSeleccionado].SetActive(true);

            var tmpRectTransform = arrayCablesConectados[indexCableSeleccionado].GetComponent<RectTransform>();
            tmpRectTransform.localPosition = arrayPositionsCablesAnillos[argIndexAnilloColocacion];
            
            indexCableSeleccionado = -1;
            refControladorSituacion5.CablesTcConectados = arrayCablesConectados[0].activeSelf && arrayCablesConectados[1].activeSelf && arrayCablesConectados[2].activeSelf;
        }

        public void DeseleccionarCable()
        {
            arrayCablesDesconectados[indexCableSeleccionado].SetActive(false);
            arrayImageCablesAnilloTransformadorEnergia[indexCableSeleccionado].SetActive(true);
            indexCableSeleccionado = -1;
        }
        #endregion
    }
}