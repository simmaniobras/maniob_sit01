using UnityEngine;
using UnityEngine.UI;

namespace NSSituacion5
{
    public class GirarAnilloTransformadorEnergia:MonoBehaviour
    {
        [SerializeField] private ControladorSituacion5 refControladorSituacion5;
        
        [SerializeField] private Image imageAnilloTransformador;

        [SerializeField] private Sprite spriteCaraS1;
        
        [SerializeField] private Sprite spriteCaraS2;

        [SerializeField] private GameObject buttonRotarDerecha;
        
        [SerializeField] private GameObject buttonRotarIzquierda;

        public void OnButtonGirarAnilloTransformador(int argIndexAnilloTransformador)
        {
            refControladorSituacion5._arrayAnillosTransformadoresCara[argIndexAnilloTransformador] = !refControladorSituacion5._arrayAnillosTransformadoresCara[argIndexAnilloTransformador];
            imageAnilloTransformador.sprite = refControladorSituacion5._arrayAnillosTransformadoresCara[argIndexAnilloTransformador]? spriteCaraS2 : spriteCaraS1;
            buttonRotarDerecha.SetActive(!refControladorSituacion5._arrayAnillosTransformadoresCara[argIndexAnilloTransformador]);
            buttonRotarIzquierda.SetActive(refControladorSituacion5._arrayAnillosTransformadoresCara[argIndexAnilloTransformador]);
        }
    }
}