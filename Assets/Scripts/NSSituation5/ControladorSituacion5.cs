﻿using System;
using System.Collections;
using NSActiveZones;
using NSBoxMessage;
using NSCelular;
using NSInterfaz;
using NSSituacion2;
using NSSituacionGeneral;
using NSTraduccionIdiomas;
using NSUtilities;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace NSSituacion5
{
    public class ControladorSituacion5 : AbstractControladorSituacion
    {
        #region members

        private bool zonaSeguridadColocada;

        private bool avatarVestido;

        [SerializeField] private Button buttonCelular;

        [SerializeField] private ControladorCelularSituacion5 refControladorCelularSituacion5;

        [SerializeField] private ActiveZonesController refActiveZonesController;

        [SerializeField] private clsRegistroDatosSituacion5 refRegistroDatosSituacion5;

        [SerializeField] private byte mensajesEnviadosCentroControl;

        [SerializeField] private ZonaActual zonaActual = ZonaActual.Null;

        [SerializeField, Header("Valores transformador")]
        private Transformador[] arrayTransformadores;

        [SerializeField] private Image imageTransformador;

        private int indexTransformadorSeleccionado;

        private float[] corrienteCablesTransformador = new float[3];

        private float[] corrienteCablesMacromedidor = new float[3];

        private float[] tensionLineaPuntasPinza = new float[3];

        private float[] tensionFasePuntasPinza = new float[3];

        private float[] potenciaMacroMedidor = new float[3];
        
        private float[] relacionTransformacion = {20, 30, 40, 80, 100};

        private float relacionTransformacionUsuario;
        
        private float corrientePantallaMacroMedidor;

        private float potenciaTransformadorActual;

        private float potenciaRegistradaExacta;
        
        
        [SerializeField] private ControladorConexionesMacroMedidor refControladorConexionesMacroMedidor;

        [SerializeField] private TextMeshProUGUI textPantallaMacroMedidor;

        [SerializeField] private ControladorTensionCortaCircuitos refControladorTensionCortaCircuitos;

        [SerializeField] private ControladorTensionTransformador refControladorTensionTransformador;
        #endregion

        #region Herramientas

        [Header("Herramientas"), Space(10)]
        [Header("Descolgar corta fusibles"), Space(10)]

        [SerializeField] private GameObject[] cortaCircuitosColgados;

        [SerializeField] private GameObject[] cortaCircuitosDescolgados;

        [SerializeField] private GameObject[] buttonsColgarCortaCircuitos;

        [SerializeField] private GameObject[] buttonsDescolgarCortaCircuitos;

        [SerializeField] private PanelInterfazSoloAnimacion refPanelInterfazSoloAnimacion;

        [SerializeField] private GameObject pertigaDescolgarFusibles;

        [SerializeField] private GameObject pertigaMensajeNoOperar;

        [SerializeField] private GameObject letreroNoOperar;

        [SerializeField] private GameObject pertigaQuitarLetrero;

        [SerializeField] private GameObject pertigaColgarFusibles;

        private bool[] cortaCircuitoEstaColgado = { true, true, true };

        [Header("Anillos transformadores"), Space(10)]
        [SerializeField] private Image[] arrayImageAnillosTransformadoresEnergia;

        [SerializeField] private GameObject[] arrayAnillosTransformadoresEnergiaConectados;

        [SerializeField] private GameObject[] arrayAnillosTransformadoresEnergiaDesconectados;
        
        private bool[] arrayAnillosTransformadoresEnergiaEstanConectados = new bool[3];

        private bool[] arrayAnillosTransformadoresCara = new bool[3];//para cada uno de los anillos transformadores se guarda la cara seleccionada, false = S1, true = S2

        [Header("Cables conexion anillos transformadores"), Space(10)]

        [SerializeField] private GameObject containerCablesAnillosTransformadoresEnergia;
        
        [Header("Herramientas recurrentes"), Space(10)]
        [SerializeField] private GameObject puntaDetectoraTension;

        [SerializeField] private GameObject pinzaAmperimetro;

        [SerializeField] private CheckCorrectTogglesActivate refCheckCorrectTogglesActivate;

        [SerializeField] private GameObject escalera;
        
        [SerializeField] private GameObject escaleraCajaHerramientas;

        /// <summary>
        /// para saber en que etapa del corta circuito se va en el uso de herramientas
        /// </summary>
        private int etapaHerramientasCortaCircuitos;
        #endregion

        #region animaciones

        [Header("Animators"), Space(10)]
        [SerializeField] private Animator animatorCortaCircuitos;

        [Header("Animators controller"), Space(10)]
        [SerializeField] private RuntimeAnimatorController animatorControllerDescolgarCortaCircuitos;

        [SerializeField] private RuntimeAnimatorController animatorControllerColgarCortaCircuitos;
        #endregion

        #region zonas activas

        [Header("Zonas Activas")]
        [SerializeField] private GameObject zonaActivaLetreroNoOperar;

        [SerializeField] private GameObject zonaActivaQuitarLetreroNoOperar;

        #endregion

        #region cortaCircuitos

        [Header("Corta circuitos afuera")]
        [SerializeField] private GameObject[] cortaCircuitosColgadosAfuera;

        [SerializeField] private GameObject[] cortaCircuitosDescolgadosAfuera;

        #endregion

        #region anillos transformadores

        /// <summary>
        /// Saber la cara actual del anillo transformador false = S1, true = S2
        /// </summary>
        public bool[] _arrayAnillosTransformadoresCara
        {
            get { return arrayAnillosTransformadoresCara; }
        }

        #endregion

        #region cables conexion anillos transformadores

        public int[] arrayIndexCableConectadoAnillosTransformadores = {-1, -1, -1};
        
        #endregion
        
        #region accesores

        public bool _zonaSeguridadColocada
        {
            set
            {
                zonaSeguridadColocada = value;
                var tmpPreparadoSituacion = zonaSeguridadColocada && avatarVestido;

                if (tmpPreparadoSituacion)
                    refControladorCelularSituacion5.SetMensaje2();
            }
            get { return zonaSeguridadColocada; }
        }

        public bool _avatarVestido
        {
            set
            {
                avatarVestido = value;
                var tmpPreparadoSituacion = zonaSeguridadColocada && avatarVestido;

                if (tmpPreparadoSituacion)
                    refControladorCelularSituacion5.SetMensaje2();
            }
            get { return avatarVestido; }
        }

        public float[] _corrienteCablesTransformador
        {
            get { return corrienteCablesTransformador; }
        }

        public float[] _corrienteCablesMacromedidor
        {
            get { return corrienteCablesMacromedidor; }
        }

        public float[] _tensionLineaPuntasPinza
        {
            get { return tensionLineaPuntasPinza; }
        }

        public float[] _tensionFasePuntasPinza
        {
            get { return tensionFasePuntasPinza; }
        }

        public bool _anillosTransformadoresEnergiaEstanConectados
        {
            get
            {
                return arrayAnillosTransformadoresEnergiaEstanConectados[0] && arrayAnillosTransformadoresEnergiaEstanConectados[1] && arrayAnillosTransformadoresEnergiaEstanConectados[2];
            }
        }

        public float RelacionTransformacion
        {
            get { return relacionTransformacion[indexTransformadorSeleccionado]; }
        }

        public float RelacionTransformacionUsuario
        {
            set
            {
                relacionTransformacionUsuario = value;
                CalcularValoresSistema();
            }
        }
        
        public bool CablesTcConectados { set; private get; }
        #endregion

        #region mono behaviour

        private void Awake()
        {
            CalcularValoresSistema();
            SetTransformadorRamdom();
            refControladorCelularSituacion5.SetMensaje1();
            refControladorConexionesMacroMedidor.OnCableConectado.AddListener(refControladorCelularSituacion5.SetMensaje3);//cada vez que conecto un cable, entonces muestro este mensaje en el celular
        }

        private void Update()
        {
            ActualizarValorMacroMedidor();
        }
        #endregion
        
        #region private methods

        private void OnButtonAceptarMensajeInformacion()
        {
            refActiveZonesController.ActiveAllZones();
        }

        private void SetTransformadorRamdom()
        {
            indexTransformadorSeleccionado = Random.Range(0, arrayTransformadores.Length);
            imageTransformador.sprite = arrayTransformadores[indexTransformadorSeleccionado].spriteTransformador;
        }

        /// <summary>
        ///  Verifica el estado de todos los corta circuitos.
        /// </summary>
        /// <returns>2 = todos los corta circuitos estan conectados, 1 = algunos corta circuitos estan conectados, 0 = ningun corta circuitos esta conectado</returns>
        private int VerificarEstadoCortaCircuitos()
        {
            var tmpCortaCircuitosColgados = 0;

            foreach (var tmpCortaCircuitos in cortaCircuitoEstaColgado)
                if (tmpCortaCircuitos)
                    tmpCortaCircuitosColgados++;

            if (tmpCortaCircuitosColgados == 3)
                return 2;
            
            if (tmpCortaCircuitosColgados == 0)
                return 0;
            
            return 1;
        }

        private void ActualizarValorMacroMedidor()
        {
            if (refControladorConexionesMacroMedidor._macromedidorConectado && !cortaCircuitosAbiertos)
                textPantallaMacroMedidor.text = corrientePantallaMacroMedidor + " KWh";
            else
                textPantallaMacroMedidor.text = "0 KWh";
        }
        #endregion

        #region public methods

        /// <summary>
        /// Asigna el indice del cable conectado a la fase X1
        /// </summary>
        /// <param name="argIndexCable">Indice del cable conectado 0 = amarillo, 1 = azul, 2 = rojo </param>
        public void SetCableAnilloTransformadorFaseX1(int argIndexCable)
        {
            arrayIndexCableConectadoAnillosTransformadores[0] = argIndexCable;
            Debug.Log("SetCableAnilloTransformadorFaseX1 : " + argIndexCable);
        }
        
        /// <summary>
        /// Asigna el indice del cable conectado a la fase X2
        /// </summary>
        /// <param name="argIndexCable">Indice del cable conectado 0 = amarillo, 1 = azul, 2 = rojo </param>
        public void SetCableAnilloTransformadorFaseX2(int argIndexCable)
        {
            arrayIndexCableConectadoAnillosTransformadores[1] = argIndexCable;
            Debug.Log("SetCableAnilloTransformadorFaseX2 : " + argIndexCable);
        }
        
        /// <summary>
        /// Asigna el indice del cable conectado a la fase X3
        /// </summary>
        /// <param name="argIndexCable">Indice del cable conectado 0 = amarillo, 1 = azul, 2 = rojo </param>
        public void SetCableAnilloTransformadorFaseX3(int argIndexCable)
        {
            arrayIndexCableConectadoAnillosTransformadores[2] = argIndexCable;
            Debug.Log("SetCableAnilloTransformadorFaseX3 : " + argIndexCable);
        }

        /// <summary>
        /// Con el indice del color del cable consigo la cara del S del cable
        /// </summary>
        /// <param name="argIndexColorCableConectado">Index del color del cable del cual se quiere saber el color de la cara</param>
        /// <returns>false = S1, true = S2</returns>
        public bool GetCaraIndexColorCableConectado(int argIndexColorCableConectado)
        {
            var tmpIndexCableEncontrado = -1;//
            
            for (int i = 0; i < arrayIndexCableConectadoAnillosTransformadores.Length; i++)
            {
                if (arrayIndexCableConectadoAnillosTransformadores[i] == argIndexColorCableConectado)
                {
                    tmpIndexCableEncontrado = i;
                    break;
                }
            }
            
            Debug.Log("El indice de cable : " + argIndexColorCableConectado + " tiene una cara S : "+arrayAnillosTransformadoresCara[tmpIndexCableEncontrado]);
            return arrayAnillosTransformadoresCara[tmpIndexCableEncontrado];
        }
        
        /// <summary>
        /// Calcula todos los valores del sistema al azar, en el awake de este script y luego en el evento de cuando se cambian todos los dps
        /// </summary>
        public void CalcularValoresSistema()
        {
            corrientePantallaMacroMedidor = 0;
            
            for (int i = 0; i < 3; i++) //X1, X2, X3
            {
                potenciaTransformadorActual = arrayTransformadores[indexTransformadorSeleccionado].potenciaKVA * 1000f;//(bien) 
                corrienteCablesTransformador[i] = (potenciaTransformadorActual / 360.26656f) * Random.Range(0.5f, 0.7f);// * Random.Range(0.6f, 0.7f);
                corrienteCablesMacromedidor[i] = (corrienteCablesTransformador[i] / relacionTransformacionUsuario) * Random.Range(0.98f, 1.02f); //(bien)
                tensionLineaPuntasPinza[i] = potenciaTransformadorActual / (1.732050f * corrienteCablesTransformador[i]);//bien
                tensionFasePuntasPinza[i] = tensionLineaPuntasPinza[i] / 1.732050f;// (bien) 
                potenciaMacroMedidor[i] = tensionFasePuntasPinza[i] * corrienteCablesMacromedidor[i] * 0.95f;// se pierde %5 (bien) //en cada indice esta lo que se deberia mostrar en cada 3 pares conectados
                corrientePantallaMacroMedidor += potenciaMacroMedidor[i] * refControladorConexionesMacroMedidor.GetSentidoCorrienteConexion3Cables(i);//suma de las potencias X1 X2 X3 (bien)
            }
        }

        public void OnButtonZonaActivaTransformador()
        {
            Debug.Log("OnButtonZonaActivaTransformador");

            if (avatarVestido)
            {
                if (zonaSeguridadColocada)
                {
                    if (escalera.activeSelf)
                    {
                        refActiveZonesController.DesactiveAllZones();
                        PanelInterfazTransformador._instance.Mostrar();
                    }
                    else
                    {
                        refActiveZonesController.DesactiveAllZones();
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeSeleccioneEscalera"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
                    }
                }
                else
                {
                    refActiveZonesController.DesactiveAllZones();
                    BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeSenializacionNoColocada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
                }
            }
            else
            {
                refActiveZonesController.DesactiveAllZones();
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeVestimentaIncorrecta"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
            }
        }

        public void OnButtonZonaActivaMacroMedidor()
        {
            Debug.Log("OnButtonZonaActivaMacroMedidor");

            if (avatarVestido)
            {
                if (zonaSeguridadColocada)
                {
                    if (!cortaCircuitosAbiertos && refControladorConexionesMacroMedidor._macromedidorConectado)//si los corta circuitos no estan abiertos y el macro medidor esta bien conectado
                    {
                        refActiveZonesController.DesactiveAllZones();
                        PanelInterfazMacroMedidor._instance.Mostrar();
                    }
                    else
                    {
                        if (!cortaCircuitosAbiertos)
                        {
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeMacromedidorNoSePuedeModificar"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
                            refActiveZonesController.DesactiveAllZones();
                        }
                        else
                        {
                            if (!CablesTcConectados)
                            {
                                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeMacromedidorNoSePuedeModificarTCNoConectados"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
                                refActiveZonesController.DesactiveAllZones();
                            }
                            else
                            {
                                refActiveZonesController.DesactiveAllZones();
                                PanelInterfazMacroMedidor._instance.Mostrar();
                            }
                        } 
                    }
                }
                else
                {
                    refActiveZonesController.DesactiveAllZones();
                    BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeSenializacionNoColocada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
                }
            }
            else
            {
                refActiveZonesController.DesactiveAllZones();
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeVestimentaIncorrecta"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
            }
        }

        public void OnButtonZonaActivaCortacircuito()
        {
            Debug.Log("OnButtonZonaActivaCortacircuito");

            if (avatarVestido)
            {
                if (!zonaSeguridadColocada)
                {
                    refActiveZonesController.DesactiveAllZones();
                    BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeSenializacionNoColocada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
                    return;
                }

                refActiveZonesController.DesactiveAllZones();
                PanelInterfazCortaCircuitos._instance.Mostrar();
            }
            else
            {
                refActiveZonesController.DesactiveAllZones();
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeVestimentaIncorrecta"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
            }
        }

        public void OnButtonValidarHerramienta()
        {
            DesactivarHerramientas();

            switch (zonaActual)
            {
                case ZonaActual.Null:
                    BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeNoPuedeUsarHerramientaAqui"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
                    PanelInterfazCajaHerramientas._instance.Mostrar(false);
                    break;

                case ZonaActual.VestirAvatar:
                    BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeNoPuedeUsarHerramientaAqui"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
                    PanelInterfazCajaHerramientas._instance.Mostrar(false);
                    break;

                case ZonaActual.Afuera:

                    if (refCheckCorrectTogglesActivate.VerifyGroupToggles("Escalera"))
                    {
                        escalera.SetActive(true);
                        escaleraCajaHerramientas.SetActive(false);
                        PanelInterfazCajaHerramientas._instance.Mostrar(false);
                    }
                    else if (refCheckCorrectTogglesActivate.AnyToggleActive())
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaSeleccionadaSinFuncion"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    else
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaNoSeleccionada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));

                    break;

                case ZonaActual.ZonaSeguridad:

                    break;

                case ZonaActual.MacroMedidor:
                    Debug.Log("EtapaSituacionActual.MacroMedidor");

                    if (refCheckCorrectTogglesActivate.VerifyGroupToggles("PinzaAmperimetrica"))
                    {
                        if (refControladorConexionesMacroMedidor._macromedidorConectado)
                        {
                            pinzaAmperimetro.SetActive(true);
                            PanelInterfazCajaHerramientas._instance.Mostrar(false);
                        }
                        else
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeMacromedidorConexionIncorrecta"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    }
                    else if (refCheckCorrectTogglesActivate.AnyToggleActive())
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaSeleccionadaSinFuncion"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    else
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaNoSeleccionada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));

                    break;

                case ZonaActual.CortaCircuitos:

                    if (refCheckCorrectTogglesActivate.VerifyGroupToggles("PuntaMedidoraTension"))
                    {
                        DesactivarHerramientas();
                        puntaDetectoraTension.SetActive(true);
                        PanelInterfazCajaHerramientas._instance.Mostrar(false);
                    }
                    else if (etapaHerramientasCortaCircuitos == 0)
                    {
                        Debug.Log("etapaHerramientasCortaCircuitos == 0");

                        if (refCheckCorrectTogglesActivate.VerifyGroupToggles("DescolgarCortaCircuitos"))
                        {
                            animatorCortaCircuitos.enabled = false;

                            foreach (var tmpButtonDescolgarCortaCircuito in buttonsDescolgarCortaCircuitos)
                                tmpButtonDescolgarCortaCircuito.SetActive(true);

                            pertigaDescolgarFusibles.SetActive(true);
                            PanelInterfazCajaHerramientas._instance.Mostrar(false);
                            etapaHerramientasCortaCircuitos++;
                        }
                        else if (refCheckCorrectTogglesActivate.AnyToggleActive())
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaInadecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                        else
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo("Debe realizar la selección de las herramientas necesarias para desenclavar el fusible.", DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    }
                    else if (etapaHerramientasCortaCircuitos == 1)
                    {
                        Debug.Log("etapaHerramientasCortaCircuitos == 1");

                        if (refCheckCorrectTogglesActivate.VerifyGroupToggles("PonerLetreroPeligroCortaCircuitos"))
                        {
                            animatorCortaCircuitos.enabled = false;
                            pertigaMensajeNoOperar.SetActive(true);
                            pertigaDescolgarFusibles.SetActive(false);
                            zonaActivaLetreroNoOperar.SetActive(true);
                            PanelInterfazCajaHerramientas._instance.Mostrar(false);
                            etapaHerramientasCortaCircuitos++;
                        }
                        else if (refCheckCorrectTogglesActivate.AnyToggleActive())
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaInadecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                        else
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeEscojaHerramientaAdecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    }
                    else if (etapaHerramientasCortaCircuitos == 2)
                    {
                        Debug.Log("etapaHerramientasCortaCircuitos == 2");

                        if (refCheckCorrectTogglesActivate.VerifyGroupToggles("QuitarLetreroNoOperar"))
                        {
                            animatorCortaCircuitos.enabled = false;
                            zonaActivaQuitarLetreroNoOperar.SetActive(true);
                            pertigaQuitarLetrero.SetActive(true);
                            PanelInterfazCajaHerramientas._instance.Mostrar(false);
                            etapaHerramientasCortaCircuitos++;
                        }
                        else if (refCheckCorrectTogglesActivate.AnyToggleActive())
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaInadecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                        else
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeEscojaHerramientaAdecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    }
                    else if (etapaHerramientasCortaCircuitos == 3)
                    {
                        Debug.Log("etapaHerramientasCortaCircuitos == 3");

                        if (refCheckCorrectTogglesActivate.VerifyGroupToggles("ColgarCortaCircuitos"))
                        {
                            animatorCortaCircuitos.enabled = false;

                            foreach (var tmpButtonColgarCortaCircuito in buttonsColgarCortaCircuitos)
                                tmpButtonColgarCortaCircuito.SetActive(true);

                            pertigaColgarFusibles.SetActive(true);
                            PanelInterfazCajaHerramientas._instance.Mostrar(false);
                            etapaHerramientasCortaCircuitos++;
                        }
                        else if (refCheckCorrectTogglesActivate.AnyToggleActive())
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaInadecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                        else
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeEscojaHerramientaAdecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    }

                    break;

                case ZonaActual.Transformador:

                    if (refCheckCorrectTogglesActivate.VerifyGroupToggles("PuntaMedidoraTension"))
                    {
                        DesactivarHerramientas();
                        puntaDetectoraTension.SetActive(true);
                        PanelInterfazCajaHerramientas._instance.Mostrar(false);
                    }
                    else if (refCheckCorrectTogglesActivate.VerifyGroupToggles("PinzaAmperimetrica"))
                    {
                        if (CablesTcConectados)
                        {
                            pinzaAmperimetro.SetActive(true);
                            PanelInterfazCajaHerramientas._instance.Mostrar(false);
                        }
                        else
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeNoSePuedeMedirTransformador"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    }
                    else if (refCheckCorrectTogglesActivate.AnyToggleActive())
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaInadecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    else
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeEscojaHerramientaAdecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    
                    break;
            }
        }

        public void DesactivarHerramientas()
        {
            puntaDetectoraTension.SetActive(false);
            pinzaAmperimetro.SetActive(false);
        }
        
        //Set etapa
        public void SetEtapaAfuera()
        {
            Debug.Log("SetEtapaVestirAvatar");
            zonaActual = ZonaActual.Afuera;
        }
        
        public void SetEtapaMacroMedidor()
        {
            Debug.Log("SetEtapaMacroMedidor");
            zonaActual = ZonaActual.MacroMedidor;
            puntaDetectoraTension.SetActive(false);
            pinzaAmperimetro.SetActive(false);
        }

        public void SetEtapaCortaCircuto()
        {
            Debug.Log("SetEtapaCortaCircuto");
            zonaActual = ZonaActual.CortaCircuitos;
            puntaDetectoraTension.SetActive(false);
            pinzaAmperimetro.SetActive(false);
        }

        public void SetEtapaTransformador()
        {
            Debug.Log("SetEtapaTransformador");
            zonaActual = ZonaActual.Transformador;
            puntaDetectoraTension.SetActive(false);
            pinzaAmperimetro.SetActive(false);
        }
        
        public void DescolgarCortaCircuito(int argIndexCortaCircuito)
        {
            cortaCircuitoEstaColgado[argIndexCortaCircuito] = false;
            buttonsDescolgarCortaCircuitos[argIndexCortaCircuito].SetActive(false);

            refPanelInterfazSoloAnimacion.Mostrar();

            animatorCortaCircuitos.enabled = true;
            animatorCortaCircuitos.runtimeAnimatorController = animatorControllerDescolgarCortaCircuitos;
            animatorCortaCircuitos.SetBool("Descolgar", true);

            StartCoroutine(CouDesactivarAnimatorDescolgarFusibles(argIndexCortaCircuito));
        }

        public void ColgarCortaCircuito(int argIndexCortaCircuito)
        {
            if (!refControladorConexionesMacroMedidor._macromedidorConectado)
            {
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeMacromedidorConexionIncorrecta"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                return;
            }
            
            cortaCircuitoEstaColgado[argIndexCortaCircuito] = true;
            buttonsColgarCortaCircuitos[argIndexCortaCircuito].SetActive(false);

            refPanelInterfazSoloAnimacion.Mostrar();
            animatorCortaCircuitos.enabled = true;
            animatorCortaCircuitos.runtimeAnimatorController = animatorControllerColgarCortaCircuitos;
            animatorCortaCircuitos.SetBool("Colgar", true);

            StartCoroutine(CouDesactivarAnimatorColgarFusibles(argIndexCortaCircuito));
        }

        public void SetLetreroNoOperarCortaCircuitos()
        {
            Debug.Log("SetLetreroNoOperarCortaCircuitos");
            pertigaMensajeNoOperar.SetActive(false);
            letreroNoOperar.SetActive(true);
        }

        public void DesactivarTodaTension(bool argDesactivarTension)
        {
            if (argDesactivarTension)
            {
                refControladorTensionCortaCircuitos.DesactivarTensionInferior();
                refControladorTensionTransformador.DesactivarTension();
            }
            else
            {
                refControladorTensionCortaCircuitos.ActivarTensionInferior();
                refControladorTensionTransformador.ActivarTension();
            }
        }

        //anillos transformador
        public void SeleccionarAnilloTransformador(int argIndexAnilloTransformador)
        {
            if (!refRegistroDatosSituacion5.ValidarCamposRelacionTransformacionLlenos())
            {
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeValorTcNecesarios"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                
                foreach (var tmpAnilloTransformadorEnergia in arrayImageAnillosTransformadoresEnergia)
                    tmpAnilloTransformadorEnergia.GetComponent<DraggableObjectUI>().CanBeDragged = false;

                return;
            }

            var tmpRelacionTransformacionValida = false;

            for (int i = 0; i < relacionTransformacion.Length; i++)
                if (Math.Abs(relacionTransformacion[i] - relacionTransformacionUsuario) < 0.1f)
                {
                    tmpRelacionTransformacionValida = true;
                    break;
                }

            if (!tmpRelacionTransformacionValida)
            {
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeValorTcNoEstaEnRango"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                
                foreach (var tmpAnilloTransformadorEnergia in arrayImageAnillosTransformadoresEnergia)
                    tmpAnilloTransformadorEnergia.GetComponent<DraggableObjectUI>().CanBeDragged = false;
                
                return;
            }
            
            foreach (var tmpAnilloTransformadorEnergia in arrayImageAnillosTransformadoresEnergia)
                tmpAnilloTransformadorEnergia.GetComponent<DraggableObjectUI>().CanBeDragged = true;

            for (int i = 0; i < arrayImageAnillosTransformadoresEnergia.Length; i++)
            {
                arrayImageAnillosTransformadoresEnergia[i].enabled = false;
                arrayImageAnillosTransformadoresEnergia[i].transform.GetChild(0).gameObject.SetActive(false);
                arrayImageAnillosTransformadoresEnergia[i].transform.GetChild(1).gameObject.SetActive(false);
            }

            arrayAnillosTransformadoresEnergiaDesconectados[argIndexAnilloTransformador].SetActive(true);
        }

        public void ConectarAnilloTransformador(int argIndexAnilloTransformador)
        {
            foreach (var tmpAnilloTransformador in arrayAnillosTransformadoresEnergiaDesconectados)
                tmpAnilloTransformador.SetActive(false);

            arrayAnillosTransformadoresEnergiaConectados[argIndexAnilloTransformador].SetActive(true);
            arrayAnillosTransformadoresEnergiaConectados[argIndexAnilloTransformador].transform.GetChild(0).gameObject.SetActive(!arrayAnillosTransformadoresCara[argIndexAnilloTransformador]); 
            arrayAnillosTransformadoresEnergiaConectados[argIndexAnilloTransformador].transform.GetChild(1).gameObject.SetActive(arrayAnillosTransformadoresCara[argIndexAnilloTransformador]); 
            arrayAnillosTransformadoresEnergiaEstanConectados[argIndexAnilloTransformador] = true;

            if (_anillosTransformadoresEnergiaEstanConectados)
                containerCablesAnillosTransformadoresEnergia.SetActive(true);

            DeseleccionarAnilloTransformador();
        }

        public void DeseleccionarAnilloTransformador()
        {
            foreach (var tmpAnilloTransformador in arrayAnillosTransformadoresEnergiaDesconectados)
                tmpAnilloTransformador.SetActive(false);

            for (int i = 0; i < arrayImageAnillosTransformadoresEnergia.Length; i++)
            {
                if (!arrayAnillosTransformadoresEnergiaEstanConectados[i])
                {
                    arrayImageAnillosTransformadoresEnergia[i].enabled = true;
                    arrayImageAnillosTransformadoresEnergia[i].transform.GetChild(0).gameObject.SetActive(!_arrayAnillosTransformadoresCara[i]);
                    arrayImageAnillosTransformadoresEnergia[i].transform.GetChild(1).gameObject.SetActive(_arrayAnillosTransformadoresCara[i]);
                }
            }
        }

        //cables
/*
        public void ConectarCableConexionAnilloTransformador(int argIndexCable)
        {
            foreach (var tmpAnilloTransformador in arrayCablesAnillosTransformadoresEnergiaDesconectados)
                tmpAnilloTransformador.SetActive(false);

            arrayCablesAnillosTransformadoresEnergiaConectados[argIndexCable].SetActive(true);
            arrayCablesAnillosTransformadoresEnergiaEstanConectados[argIndexCable] = true;
            Debug.Log("argIndexCable : " + argIndexCable);
            DeseleccionarCableConexionAnilloTransformador();
        }

        public void DeseleccionarCableConexionAnilloTransformador()
        {
            foreach (var tmpAnilloTransformador in arrayCablesAnillosTransformadoresEnergiaDesconectados)
                tmpAnilloTransformador.SetActive(false);

            for (int i = 0; i < arrayImageAnillosTransformadoresEnergia.Length; i++)
            {
                if (!arrayCablesAnillosTransformadoresEnergiaEstanConectados[i])
                    arrayImageCablesAnillosTransformadoresEnergia[i].enabled = true;
            }
        }*/

        #endregion

        #region courutines

        private IEnumerator CouDesactivarAnimatorDescolgarFusibles(int argIndexCortaCircuito)
        {
            pertigaDescolgarFusibles.SetActive(false);
            yield return new WaitForSeconds(3);

            cortaCircuitosColgados[argIndexCortaCircuito].SetActive(false);
            cortaCircuitosDescolgados[argIndexCortaCircuito].SetActive(true);
            
            animatorCortaCircuitos.enabled = false;

            cortaCircuitosDescolgadosAfuera[argIndexCortaCircuito].SetActive(true);
            cortaCircuitosColgadosAfuera[argIndexCortaCircuito].SetActive(false);
            
            refPanelInterfazSoloAnimacion.Mostrar(false);

            if (VerificarEstadoCortaCircuitos() == 0)
            {
                etapaHerramientasCortaCircuitos = 1;
                cortaCircuitosAbiertos = true;
                DesactivarTodaTension(true);
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeColocarLetreroParaContinuar"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
            }
            else
                pertigaDescolgarFusibles.SetActive(true);

            Debug.Log("CouDesactivarAnimatorDescolgarFusibles");
        }

        private IEnumerator CouDesactivarAnimatorColgarFusibles(int argIndexCortaCircuito)
        {
            pertigaColgarFusibles.SetActive(false);
            yield return new WaitForSeconds(3.25f);
            
            cortaCircuitosDescolgados[argIndexCortaCircuito].SetActive(false);
            cortaCircuitosColgados[argIndexCortaCircuito].SetActive(true);
            
            animatorCortaCircuitos.enabled = false;
            pertigaDescolgarFusibles.SetActive(false);

            cortaCircuitosDescolgadosAfuera[argIndexCortaCircuito].SetActive(false);
            cortaCircuitosColgadosAfuera[argIndexCortaCircuito].SetActive(true);

            refControladorCelularSituacion5.SetMensaje4();
            refPanelInterfazSoloAnimacion.Mostrar(false);

            if (VerificarEstadoCortaCircuitos() == 2)
            {
                etapaHerramientasCortaCircuitos = 0;
                cortaCircuitosAbiertos = false;
                CalcularValoresSistema();
                DesactivarTodaTension(false);
            }
            else
                pertigaColgarFusibles.SetActive(true);

            Debug.Log("CouDesactivarAnimatorColgarFusibles finish");
        }
        #endregion

        #region MyRegion
        
        #endregion
    }

    [Serializable]
    public class Transformador
    {
        public float potenciaKVA;

        public Sprite spriteTransformador;
    }
    
    public enum ZonaActual
    {
        Null,
        VestirAvatar,
        ZonaSeguridad,
        MacroMedidor,
        CortaCircuitos,
        Transformador,
        Afuera
    }
}